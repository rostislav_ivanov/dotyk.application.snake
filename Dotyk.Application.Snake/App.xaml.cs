﻿using System.Diagnostics;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Dotyk.Application;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Dotyk.Application.Snake
{
    sealed partial class App : DotykApplication
    {
        public static string Args;

        public App() : base(false)
        {
            InitializeComponent();
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        {
            base.OnLaunched(e);

            await Initialized;

            OnFrameContentReady(); //if this is called after 2 second your app will be closed

            
            if (RootFrame.Content == null)
                RootFrame.Navigate(typeof(MainPage), e.Arguments);

            RootFrame.Background = new SolidColorBrush(Colors.Transparent);
            Window.Current.CoreWindow.Activate();

            Args = e.Arguments;
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            base.OnWindowCreated(args);
        }
    }
}
