﻿using Dotyk.Shell.Utils;
using System;
using System.Collections.Generic;
using System.Numerics;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public enum SnakeState { Go, Touched, Stucked, Captured, Released, TeleportStarted, Teleporting, TeleportFinished, StuckedDuringTeleportation };

    public enum ObstacleType { AllClear, Table, AnotherSnake, SnakeItself, Real };

    public class AvoidObstaclesAlgorithm
    {
        public static Color[] Obstacles;
        public static Color[] TouchSimulationObstacales;
        public static Color ObstacleColor;
        public static Color EmptySpaceColor;
        public static Color AppObstacleColor = Colors.Green;

        SnakeModel snakeModel;
        int w;
        int h;

        int iterationsCount;
        float[] atemptsOrder;
        int countGoodAngles = 11;

        bool isObsticaleHited;
        int spinsCount = 0;
        int defSpinsCount = 2;

        int sign = 1;
        float minX, maxX = 0, minY, maxY = 0;
        byte moveNumber = 0;

        public List<SnakePartPath> route;
        double currentAngle;
        double initialAngleToMoveAwayFromObstacle = 0;

        List<MinMaxRegion> mimMaxRegions;
        bool tryToMoveOutOfSmallRegion;
        float moveOutOfSmallRegionAttempts = 0;
        int countMinMaxRegions = 40;
        int maxOutOfSmallRegionAttempts = 120; //before teleport

        bool[,] hasBeenHere;
        bool tryToMoveInNewSpace;
        int movesInNewSpace = 0;
        int minNewSpaceBeforeRefresh = 0;
        int hasBeenHereCellSize;
        int sizeOfSmallRegion = 20; // (w * h / sizeOfSmallRegion)

        public SnakeState State;

        bool tryToEat;
        int beforeTeleport = 0;

        

        public AvoidObstaclesAlgorithm(SnakeModel snake, int width, int height, int iterationsToFindAngle)
        {
            snakeModel = snake;
            
            w = width;
            h = height;
            iterationsCount = iterationsToFindAngle;

            atemptsOrder = new float[2 * iterationsToFindAngle];

            SetFirstAngles(false);

            for (int i = countGoodAngles; i < atemptsOrder.Length; i++)
            {
                if(i % 2 != 0)
                    atemptsOrder[i] = (i + 1) / 2;
                else
                    atemptsOrder[i] = -(i + 1) / 2;
            }

            minX = width;
            minY = height;

            mimMaxRegions = new List<MinMaxRegion>();

            hasBeenHereCellSize = 2 * snakeModel.PartDiameter;

            RefreshNewSpaces();

            minNewSpaceBeforeRefresh = 2 * ((w / hasBeenHereCellSize - 2) * (h / hasBeenHereCellSize - 2)) / 3;
        }
       
        public static bool IsTeleporting(SnakeState state)
        {
            return state == SnakeState.TeleportStarted || state == SnakeState.Teleporting || state == SnakeState.TeleportFinished || state == SnakeState.StuckedDuringTeleportation;
        }

        private void SetFirstAngles(bool isDefault)
        {
            float snakeSizeKoeff = 0;// snakeModel.Body == null ? 2 : 2*((snakeModel.Body.Count / snakeModel.MinLength) - 1f);

            atemptsOrder[0] =   snakeSizeKoeff + 1f;
            atemptsOrder[1] =  -(snakeSizeKoeff + 1f);

            atemptsOrder[2] =   snakeSizeKoeff + 2f;
            atemptsOrder[3] =  -(snakeSizeKoeff + 2f);

            atemptsOrder[4] =   snakeSizeKoeff + 3f;
            atemptsOrder[5] =  -(snakeSizeKoeff + 3f);

            atemptsOrder[6] =  (snakeSizeKoeff + 4f);
            atemptsOrder[7] =  -(snakeSizeKoeff + 4f);

            atemptsOrder[8] = (snakeSizeKoeff + 5f);
            atemptsOrder[9] = -(snakeSizeKoeff + 5);
            atemptsOrder[10] = 0;

            if (!isDefault)
            {
                byte rand_index1 = (byte)Math.Round(Utils.Rand.NextDouble() * 10);
                byte rand_index2 = (byte)Math.Round(Utils.Rand.NextDouble() * 10);

                float tmp = atemptsOrder[rand_index1];
                atemptsOrder[rand_index1] = atemptsOrder[rand_index2];
                atemptsOrder[rand_index2] = tmp;
            }
        }

        private void Randomize()
        {
            if(Utils.Rand.NextDouble() > 0.99)
                SetFirstAngles(false);
            else if(Utils.Rand.NextDouble() < 0.01)
                SetFirstAngles(true);
        }

        int continueAngle = 0;
        int arcLentgh = 4;
        public SnakePartPath Run(double curAngle)
        {
            if(Obstacles == null)
            {
                throw new Exception("You should set obstacles first");
            }

            if (snakeModel.Body == null || snakeModel.Body.Count == 0)
            {
                throw new Exception("snake not initialized");
            }

           
            this.currentAngle = Double.IsNaN(curAngle) ? snakeModel.Body.First.Value.CurrPos.Angle : curAngle;
           
            /*
            continueAngle++;
            SnakePartPath tmpPath;

            double tmpAngle = curAngle + atemptsOrder[continueAngle / 10] * Math.PI / iterationsCount;

            route = new List<SnakePartPath>();
            tmpPath = snakeModel.AngleToNewHeadPos(tmpAngle, null);

            route.Add(tmpPath);
            if (Check(tmpPath.Position, 0, snakeModel.ObstacleColorForOther))
            {
                return EndRun(SnakeState.Go);
            }
            else
            {
                return EndRun(SnakeState.Stucked);
            }
           */
           
            if (!IsTeleporting(State))
            {
                if (CheckIsTouched())
                    return EndRun(SnakeState.Touched);
            }

            Randomize();

            if (State == SnakeState.Stucked)
            {
                curAngle = Utils.Rand.NextDouble() * Math.PI;
            }

            float squareRatio = IsRegionTooSmall();

            if (squareRatio > 1 && State != SnakeState.Teleporting)
            {
                moveOutOfSmallRegionAttempts += squareRatio;
            }
            else
            {
                moveOutOfSmallRegionAttempts = 0;
            }

            if (State != SnakeState.Teleporting && moveOutOfSmallRegionAttempts > maxOutOfSmallRegionAttempts)
            {
                return EndRun(SnakeState.Captured);
            }

            bool isNearObstacle = false;
            //if (!snakeModel.IsGoodSnake)
            {
                double angleToObstacle = IsThereAreAnyObstaclesNear();

                if (!Double.IsNaN(angleToObstacle))
                {
                    this.currentAngle = angleToObstacle - Math.PI;
                    isNearObstacle = true;
                }
            }
            /*else
            {
                double angleToObstacle = IsThereAreAnyObstaclesNearGoodSnake();

                if (!Double.IsNaN(angleToObstacle))
                {
                    curAngle = angleToObstacle;
                }
            }*/

            bool additionalConditions = false;
            if (route != null && route.Count > 1 &&
                isNearObstacle)
            {
                route.RemoveAt(0);
                tryToEat = true;
                tryToMoveInNewSpace = true;
                if (CheckRouteAndAddLastAngle(out additionalConditions))
                {
                    if (additionalConditions)
                        return EndRun(SnakeState.Go);
                }
                tryToEat = false;
                tryToMoveInNewSpace = false;
            }

            if (!snakeModel.IsGoodSnake && Food.IsAnyFoodNear(snakeModel.HeadPosition, arcLentgh * snakeModel.MinMov + snakeModel.PartDiameter * 1.5f))
            {
                tryToEat = true;
                if (FindRoute(true, 0, countGoodAngles, snakeModel.Radius + 3))
                {
                    return EndRun(SnakeState.Go);
                }
                tryToEat = false;
            }

            if (Food.IsAnyFoodNear(snakeModel.HeadPosition, arcLentgh * snakeModel.MinMov + snakeModel.PartDiameter * 1.5f))
                tryToEat = true;

            tryToMoveInNewSpace = true;
            tryToMoveOutOfSmallRegion = true;
            if (FindRoute(true, 0, countGoodAngles, snakeModel.Radius + 3))
            {
                return EndRun(SnakeState.Go);
            }
            tryToMoveInNewSpace = false;
            tryToMoveOutOfSmallRegion = false;


            if (FindRoute(false, 0, countGoodAngles))
            {
                return EndRun(SnakeState.Go);
            }

            if (FindRoute(false, 0, atemptsOrder.Length - 1))
            {
                return EndRun(SnakeState.Go);
            }

            return EndRun(SnakeState.Stucked);
            /*

            */
            /*
            //To make snake moves away from obstacle
            
            if (!snakeModel.IsGoodSnake && Food.IsAnyFoodNear(snakeModel.HeadPosition, arcLentgh * snakeModel.MinMov))
            {
                tryToEat = true;
                if (CheckAllGoodAngles(arcLentgh))
                {
                    return EndRun(SnakeState.Go);
                }
                tryToEat = false;
            }

            tryToMoveInNewSpace = true;
            if (CheckAllGoodAngles(arcLentgh))
            {
                return EndRun(SnakeState.Go);
            }
            tryToMoveInNewSpace = false;
            

            tryToMoveOutOfSmallRegion = true;
            if (CheckAllGoodAngles(arcLentgh))
            {
                return EndRun(SnakeState.Go);
            }
            tryToMoveOutOfSmallRegion = false;
            
            if(!CheckAllGoodAngles(arcLentgh))
            {
                if(!CheckAllPossibleWays(arcLentgh))
                    return EndRun(SnakeState.Stucked);
            }
            
            return EndRun(SnakeState.Go);
            */
        }

        private double IsThereAreAnyObstaclesNear()
        {
            initialAngleToMoveAwayFromObstacle = 0;

            double angleEncounter =  Math.PI / 4;
            int[] attemptAngles = new int[] { 1, 2, 3, -1, -2, -3 };

            Vector2 attemptPos;
            Vector2 attemptOppositePos;
            int dist = 1;

            for (int i = 0; i < attemptAngles.Length; i++)
            {

                //for (int dist = 1; dist <= 2; dist++)
                {
                    //if (ObstacleType.Real == CheckObstacleType(snakeModel.AngleToNewHeadPosEstimation(curAngle + angleEncounter * attemptAngles[i], dist * snakeModel.PartDiameter).Position, 2, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther) &&
                    //    ObstacleType.AllClear == CheckObstacleType(snakeModel.AngleToNewHeadPosEstimation(curAngle + angleEncounter * attemptAngles[i] - Math.PI, snakeModel.PartDiameter).Position, 2, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther))
                    if (ObstacleType.Real == CheckObstacleType(new Vector2((float)(snakeModel.HeadPosition.X + Math.Cos(attemptAngles[i] * angleEncounter) * dist * snakeModel.PartDiameter),
                        (float)(snakeModel.HeadPosition.Y + Math.Sin(attemptAngles[i] * angleEncounter) * dist * snakeModel.PartDiameter)),
                        snakeModel.Radius, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther) &&
                        ObstacleType.AllClear == CheckObstacleType(new Vector2((float)(snakeModel.HeadPosition.X + Math.Cos(attemptAngles[i] * angleEncounter - Math.PI) * dist * snakeModel.PartDiameter),
                        (float)(snakeModel.HeadPosition.Y + Math.Sin(attemptAngles[i] * angleEncounter - Math.PI) * dist * snakeModel.PartDiameter)),
                        snakeModel.Radius, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther))
                    {
                        return angleEncounter * attemptAngles[i];
                    }
                }
            }

            return Double.NaN;
        }

        private double IsThereAreAnyObstaclesNearGoodSnake()
        {
            initialAngleToMoveAwayFromObstacle = 0;

            double angleEncounter = Math.PI / 4;
            int[] attemptAngles = new int[] { 1, 2, 3, -1, -2, -3 };

            Vector2 attemptPos;
            Vector2 attemptOppositePos;

            for (int i = 0; i < attemptAngles.Length; i++)
            {
                for (int dist = 1; dist <= 2; dist++)
                {
                    //if (ObstacleType.Real == CheckObstacleType(snakeModel.AngleToNewHeadPosEstimation(curAngle + angleEncounter * attemptAngles[i], dist * snakeModel.PartDiameter).Position, 2, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther))
                    if (ObstacleType.Real == CheckObstacleType(new Vector2((float)(snakeModel.HeadPosition.X + Math.Cos(attemptAngles[i] * angleEncounter) * dist * snakeModel.PartDiameter),
                                                                           (float)(snakeModel.HeadPosition.Y + Math.Sin(attemptAngles[i] * angleEncounter) * dist * snakeModel.PartDiameter)),
                                                                           snakeModel.Radius, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther))
                    {
                        return angleEncounter * attemptAngles[i];
                    }
                }
            }

            return Double.NaN;
        }

        private float IsRegionTooSmall()
        {
            mimMaxRegions.Add(snakeModel.GetWindow());

            if (mimMaxRegions.Count > countMinMaxRegions)
            {
                mimMaxRegions.RemoveAt(0);
            }

            minX = w;
            minY = h;
            maxX = 0;
            maxY = 0;
            foreach (var region in mimMaxRegions)
            {
                if (region.MinPoint.X < minX)
                {
                    minX = region.MinPoint.X;
                }

                if (region.MinPoint.Y < minY)
                {
                    minY = region.MinPoint.Y;
                }

                if (region.MaxPoint.X > maxX)
                {
                    maxX = region.MaxPoint.X;
                }

                if (region.MaxPoint.Y > maxY)
                {
                    maxY = region.MaxPoint.Y;
                }
            }

            return mimMaxRegions.Count < countMinMaxRegions ? 0 : (w * h / sizeOfSmallRegion) / ((maxX - minX) * (maxY - minY));
        }

        private SnakePartPath EndRun(SnakeState newState)
        {
            tryToEat = false;
            tryToMoveOutOfSmallRegion = false;
            tryToMoveInNewSpace = false;

            if(route != null && route.Count > 1)
                SetHasBeenHere(route[0].Position);

            if (State == SnakeState.TeleportStarted)
            {
                snakeModel.ChangeSizeAfterTeleport();
                State = SnakeState.Teleporting;
                return route[0];
            }

            if (State == SnakeState.Teleporting && newState == SnakeState.Go)
            {
                if (Vector2.Distance(snakeModel.Body.Last.Previous.Value.CurrPos.Position, snakeModel.TeleportExitPos) < snakeModel.PartDiameter * 4)
                {
                    State = SnakeState.TeleportFinished;
                }

                return route[0];
            }

            if (IsTeleporting(State) && newState == SnakeState.Stucked)
            {
                State = SnakeState.StuckedDuringTeleportation;
                snakeModel.ChangeSizeRemoveWhatIsNoTeleported();
                return StartTeleport();
            }

            if (State == newState)
            {
                if (State == SnakeState.Captured)
                {
                    return StartTeleport();
                }
                else if (State == SnakeState.Stucked)
                {
                    beforeTeleport++;

                    if (snakeModel.IsGoodSnake && beforeTeleport > 9 ||
                        !snakeModel.IsGoodSnake && beforeTeleport > 3)
                    {
                        beforeTeleport = 0;
                        return StartTeleport();
                    }
                }
                else if (State == SnakeState.Touched)
                {
                    beforeTeleport++;

                    if (snakeModel.IsGoodSnake && beforeTeleport > 15 ||
                        !snakeModel.IsGoodSnake && beforeTeleport > 3)
                    {
                        beforeTeleport = 0;
                        return StartTeleport();
                    }
                }
            }
            else
            {
                if (State == SnakeState.Touched && newState == SnakeState.Go)
                    State = SnakeState.Released;
                else
                    State = newState;
            }

            if (route == null)
            {
                return new SnakePartPath();
            }
            else if (State == SnakeState.Touched || State == SnakeState.Stucked)
            {
                route[0].Angle = currentAngle;
                return route[0];
            }
            else
            {
                return route[0];
            }
        }

        private SnakePartPath StartTeleport()
        {
            moveOutOfSmallRegionAttempts = 0;
            State = SnakeState.TeleportStarted;
            var returnPath = Dig();
            snakeModel.TeleportEnterPos = snakeModel.HeadPosition;
            snakeModel.TeleportExitPos = returnPath.Position;
            return returnPath;
        }

        private bool FindRoute(bool withConditions, int beginIndex, int endIndex, int radius = 0)
        {
            route = new List<SnakePartPath>(arcLentgh);
            for (int z = 0; z < arcLentgh;  z++)
                route.Add(new SnakePartPath());


            double tmpAngle, initAngle = currentAngle;
            bool tmpBool;
            SnakePartPath tmpPath;

            for (int i = beginIndex; i <= endIndex; i++)
            {
                tmpAngle = atemptsOrder[i] * Math.PI / iterationsCount;
                tmpPath = snakeModel.AngleToNewHeadPos(initAngle + tmpAngle, null);

                if (CheckMoveForward(tmpPath, out tmpBool, radius))
                {
                    route[0] = tmpPath;
                    route[0].AngleIndex = i;
                    route[0].AngleDelta = tmpAngle;
                    route[0].Condition = tmpBool;

                    for (int j = beginIndex; j <= endIndex; j++)
                    {
                        tmpAngle = atemptsOrder[j] * Math.PI / iterationsCount;
                        tmpPath = snakeModel.AngleToNewHeadPos(initAngle + route[0].AngleDelta + tmpAngle, route[0]);

                        if (CheckMoveForward(tmpPath, out tmpBool, radius))
                        {
                            route[1] = tmpPath;
                            route[1].AngleIndex = j;
                            route[1].AngleDelta = tmpAngle;
                            route[1].Condition = tmpBool;

                            for (int k = beginIndex; k <= endIndex; k++)
                            {
                                tmpAngle = atemptsOrder[k] * Math.PI / iterationsCount;
                                tmpPath = snakeModel.AngleToNewHeadPos(initAngle + route[0].AngleDelta + route[1].AngleDelta + tmpAngle, route[1]);

                                if (CheckMoveForward(tmpPath, out tmpBool, radius))
                                {
                                    route[2] = tmpPath;
                                    route[2].AngleIndex = k;
                                    route[2].AngleDelta = tmpAngle;
                                    route[2].Condition = tmpBool;

                                    for (int v = beginIndex; v <= endIndex; v++)
                                    {
                                        tmpAngle = atemptsOrder[v] * Math.PI / iterationsCount;
                                        tmpPath = snakeModel.AngleToNewHeadPos(initAngle + route[0].AngleDelta + route[1].AngleDelta + route[2].AngleDelta + tmpAngle, route[2]);

                                        if (CheckMoveForward(tmpPath, out tmpBool, radius))
                                        {
                                            route[3] = tmpPath;
                                            route[3].AngleIndex = v;
                                            route[3].AngleDelta = tmpAngle;
                                            route[3].Condition = tmpBool;

                                            if (!withConditions)
                                                return true;
                                            else if (route[0].Condition || route[1].Condition || route[2].Condition || route[3].Condition)
                                                return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            return false;
        }

        private bool CheckRouteAndAddLastAngle(out bool additionalCondition)
        {
            bool anyTrue = false;

            foreach (var point in route)
            {
                if (!CheckMoveForward(point, out additionalCondition))
                    return false;

                anyTrue |= additionalCondition;
            }

            SnakePartPath tmpPath;
            double tmpAngle;

            if (route[route.Count - 1].AngleIndex < countGoodAngles)
            {
                tmpAngle = route[route.Count - 1].Angle + atemptsOrder[route[route.Count - 1].AngleIndex] * Math.PI / iterationsCount;
                tmpPath = snakeModel.AngleToNewHeadPos(tmpAngle, route[route.Count - 1]);
                if (CheckMoveForward(tmpPath, out additionalCondition))
                {
                    tmpPath.AngleIndex = route[route.Count - 1].AngleIndex;
                    route.Add(tmpPath);
                    anyTrue |= additionalCondition;
                    additionalCondition = anyTrue;
                    return true;
                }
            }

            for (int m = 0; m < countGoodAngles; m++)
            {
                tmpAngle = route[route.Count - 1].Angle + atemptsOrder[m] * Math.PI / iterationsCount;

                tmpPath = snakeModel.AngleToNewHeadPos(tmpAngle, route[route.Count - 1]);

                if (CheckMoveForward(tmpPath, out additionalCondition))
                {
                    tmpPath.AngleIndex = m;
                    route.Add(tmpPath);
                    anyTrue |= additionalCondition;
                    additionalCondition = anyTrue;
                    return true;
                }
            }

            additionalCondition = anyTrue;
            return false;
        }

        //move to static
        public SnakePartPath Dig()
        {
            float marginToBorder = snakeModel.Radius * 4;

            SnakePartPath newTeleport = new SnakePartPath(0, (float)Utils.Rand.NextDouble() * 2 * Math.PI);
            if (minX > maxX || minY > maxY)
            {
                for (int i = 0; i < 50; i++)
                {
                    newTeleport = new SnakePartPath(new Vector2((float)Utils.Rand.NextDouble() * (w - 2 * marginToBorder) + marginToBorder, 
                                                                (float)Utils.Rand.NextDouble() * (h - 2 * marginToBorder) + marginToBorder),
                                                                snakeModel.MinMov,
                                                                (float)Utils.Rand.NextDouble() * 2 * Math.PI);
                    if (Check(newTeleport.Position, snakeModel.Radius * 3) && !Food.IsAnyFoodNear(newTeleport.Position, snakeModel.Radius * 2))
                        return newTeleport;
                }
            }


            for (int i=0; i < 150; i++)
            {
                float newX = (float)Utils.Rand.NextDouble() * (w - (maxX - minX));
                float newY = (float)Utils.Rand.NextDouble() * (h - (maxY - minY));

                newTeleport = new SnakePartPath(new Vector2(Math.Min(w - marginToBorder, Math.Max(marginToBorder, newX > minX ? newX + (maxX - minX) : newX)),
                                                            Math.Min(h - marginToBorder, Math.Max(marginToBorder, newY > minY ? newY + (maxY - minY) : newY))),
                                                            snakeModel.MinMov, 
                                                            (float)Utils.Rand.NextDouble() * 2 * Math.PI);

                if (i < 50 && GetHasBeenHere(newTeleport.Position))
                   continue;

                if (i < 100 && Check(newTeleport.Position, snakeModel.Radius * 3) && !Food.IsAnyFoodNear(newTeleport.Position, snakeModel.Radius * 2))
                    return newTeleport;

                if (i >= 100 && Check(newTeleport.Position, snakeModel.Radius * 2) && !Food.IsAnyFoodNear(newTeleport.Position, snakeModel.Radius))
                    return newTeleport;
            }

            return newTeleport;
        }

        private bool CheckMoveForward(SnakePartPath pos, out bool additionalCondition, int radius = 0)
        {
            additionalCondition = false;

            if (tryToEat && Food.IsAnyFoodNear(pos.Position, snakeModel.PartDiameter * 1.5f))
            {
                additionalCondition = true;
            }
            else if (tryToMoveInNewSpace && !GetHasBeenHere(pos.Position))
            {
                additionalCondition = true;
            }
            else if (tryToMoveOutOfSmallRegion && (pos.Position.X > maxX || pos.Position.X < minX || pos.Position.Y > maxY || pos.Position.Y < minY))
            {
                additionalCondition = true;
            }

            return Check(pos.Position, radius, snakeModel.ObstacleColorForOther);
        }

        private bool Check(Vector2 pos, int newRadius = 0, Color? ignoreColor1 = null, Color? ignoreColor2 = null)
        {
            if (CheckObstacleType(pos, newRadius, ignoreColor1, ignoreColor2) == ObstacleType.AllClear)
                return true;
            else
                return false;
        }    

        private ObstacleType CheckObstacleType(Vector2 pos, int newRadius = 0, Color? ignoreColor1 = null, Color? ignoreColor2 = null)
        {
            int r = newRadius == 0 ? snakeModel.Radius + 1 : newRadius;

            int nhx = (int)Math.Round(pos.X); //newHeadPosPixelX
            int nhy = (int)Math.Round(pos.Y); //newHeadPosPixelY

            if (nhx > r && nhx < (w - r) &&
                nhy > r && nhy < (h - r))
            {
                for (int i = -r; i <= r; i++)
                {
                    for (int j = -r; j <= r; j++)
                    {
                        if (i * i + j * j <= r * r)
                        {
                            Color color = Obstacles[(nhy + i) * w + nhx + j];
                            if (color != EmptySpaceColor && color != ignoreColor1 && color != ignoreColor2)
                            {
                                if (color == AvoidObstaclesAlgorithm.ObstacleColor)
                                    return ObstacleType.Real;
                                else if (color == snakeModel.ObstacleColorForMyself)
                                    return ObstacleType.SnakeItself;
                                else
                                    return ObstacleType.AnotherSnake;
                            }
                        }
                    }
                }
                return ObstacleType.AllClear;
            }

            return ObstacleType.Table;
        }

        public static bool Check(Vector2 pos, int r)
        {
            int nhx = (int)Math.Round(pos.X); //newHeadPosPixelX
            int nhy = (int)Math.Round(pos.Y); //newHeadPosPixelY

            if (nhx > r && nhx < (Touch.RealWidth - r) &&
                nhy > r && nhy < (Touch.RealHeight - r))
            {
                for (int i = -r; i <= r; i++)
                {
                    for (int j = -r; j <= r; j++)
                    {
                        if (i * i + j * j <= r * r)
                        {
                            Color color = Obstacles[(nhy + i) * Touch.RealWidth + nhx + j];
                            if (color != EmptySpaceColor)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }

            return false;
        }


        public static bool CheckNotUnderApp(Vector2 pos, int r)
        {
            int nhx = (int)Math.Round(pos.X); //newHeadPosPixelX
            int nhy = (int)Math.Round(pos.Y); //newHeadPosPixelY

            if (nhx > r && nhx < (Touch.RealWidth - r) &&
                nhy > r && nhy < (Touch.RealHeight - r))
            {
                for (int i = -r; i <= r; i++)
                {
                    for (int j = -r; j <= r; j++)
                    {
                        if (i * i + j * j <= r * r)
                        {
                            Color color = Obstacles[(nhy + i) * Touch.RealWidth + nhx + j];
                            if (color == AvoidObstaclesAlgorithm.AppObstacleColor)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }

            return false;
        }


        private bool GetHasBeenHere(Vector2 pos)
        {
            try
            {
                return hasBeenHere[(int)(pos.X / hasBeenHereCellSize), (int)(pos.Y / hasBeenHereCellSize)];
            }
            catch(Exception e)
            {
                return false;
            }
        }

        private void SetHasBeenHere(Vector2 pos)
        {
            if (!GetHasBeenHere(pos))
            {
                hasBeenHere[(int)(pos.X / hasBeenHereCellSize), (int)(pos.Y / hasBeenHereCellSize)] = true;

                movesInNewSpace++;
                if (movesInNewSpace > minNewSpaceBeforeRefresh)
                {
                    RefreshNewSpaces();
                }
            }

        }

        private void RefreshNewSpaces()
        {
            hasBeenHere = new bool[w / hasBeenHereCellSize + 1, h / hasBeenHereCellSize + 1];

            for(int i=0; i < w / hasBeenHereCellSize + 1; i++)
            {
                hasBeenHere[i, 0] = true;
                hasBeenHere[i, h / hasBeenHereCellSize] = true;
            }

            for (int i = 0; i < h / hasBeenHereCellSize + 1; i++)
            {
                hasBeenHere[0, i] = true;
                hasBeenHere[w / hasBeenHereCellSize, i] = true;
            }

            movesInNewSpace = 0;
        }

        public bool CheckCurrentRoute()
        {
            if (route.Count < 2)
                return false;

            bool additionalCondition;
            foreach (var point in route)
            {
                if (!CheckMoveForward(point, out additionalCondition))
                    return true;
            }

            return false;
        }

        public bool CheckIfTouchedStateIsChanged()
        {
            if (AvoidObstaclesAlgorithm.IsTeleporting(State))
                return false;

            bool isTouched = CheckIsTouched();
            if (State == SnakeState.Touched && !isTouched ||
                State != SnakeState.Touched && isTouched)
                return true;

            return false;
        }

        public bool CheckIsTouched()
        {
            if (snakeModel.Body == null || snakeModel.Body.Count == 0)
            {
                return false;
            }

            LinkedListNode<SnakePart> bodyPart = snakeModel.Body.First;

            int k = 0;
            while (k < snakeModel.Body.Count && bodyPart != null)
            {
                //ToDo: for smooth snake
                //if (k % snakeModel.CountOfBodyPartsToNextTanged != 0)
                //    continue;

                if (CheckObstacleType(bodyPart.Value.CurrPos.Position, snakeModel.Radius, snakeModel.ObstacleColorForMyself, snakeModel.ObstacleColorForOther) == ObstacleType.Real)
                    return true;

                k++;
                bodyPart = bodyPart.Next;
            }

            return false;
        }

        public static void AddObstacle(Vector2 position, int r, Color color)
        {
            int pX = (int)Math.Round(position.X);
            int pY = (int)Math.Round(position.Y);

            int npX, npY;

            for (int i = -r; i <= r; i++)
            {
                for (int j = -r; j <= r; j++)
                {
                    if (i * i + j * j <= r * r)
                    {
                        npX = pX + j;
                        npY = pY + i;

                        if (npY < Touch.RealHeight && npX < Touch.RealWidth && npX >= 0 && npY >= 0 && Obstacles[npY * Touch.RealWidth + (pX + j)] != ObstacleColor)
                            Obstacles[npY * Touch.RealWidth + (pX + j)] = color;
                    }
                }
            }
        }

        public static void AddObstacle(Vector2 position, Vector2 widthHeight, Color color)
        {
            int leftTopX = (int)(position.X);
            int leftTopY = (int)(position.Y);
            int width    = (int)(widthHeight.X);
            int height   = (int)(widthHeight.Y);

            if (leftTopX < 0)
                leftTopX = 0;

            if (leftTopY < 0)
                leftTopY = 0;

            if (leftTopX >= Touch.RealWidth || leftTopY >= Touch.RealHeight)
                return;

            if (leftTopX + width >=  Touch.RealWidth)
                width = Touch.RealWidth   - leftTopX - 1;

            if (leftTopY + height >= Touch.RealHeight)
                height = Touch.RealHeight - leftTopY - 1;

            int index;
            for (int i = leftTopX; i < leftTopX + width; i++)
            {
                for (int j = leftTopY; j < leftTopY + height; j++)
                {
                    index = j * Touch.RealWidth + i;

                    if (Obstacles[index] != ObstacleColor)
                        Obstacles[index] = color;
                }
            }
        }
    }
}
