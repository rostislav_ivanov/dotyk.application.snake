﻿using Microsoft.Graphics.Canvas;
using System;
using System.Numerics;
using Windows.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Dotyk.Application.Snake
{
    public static class Food
    {
        public static byte FoodCount = 7;
        public static FoodItem[] FoodCollection;
        public static CanvasBitmap[] foodBitmap;
        public static CanvasBitmap[] foodBitmap_;
        public static CanvasBitmap[] foodShadowBitmap;
        public static CanvasBitmap shadowBitmap;
        public static List<byte> NotUsedColors;

        public static void InitializeFoodCollection()
        {
            FoodCollection = new FoodItem[FoodCount];
            NotUsedColors = new List<byte>();

            for (byte i = 0; i < PossibleColors.Length; i++)
            {
                NotUsedColors.Add(i);
            }

            for (int i = 0; i < FoodCount; i++)
            {
                FoodCollection[i] = new FoodItem((byte)i, new Vector2((float)((i + 1) * Touch.RealWidth / 10 + Touch.RealWidth / 4 + (i % 2) * (Touch.RealWidth / 4)),
                                                                      (float)((i + 1) * Touch.RealHeight / 10 + Touch.RealHeight / 4)), (float)(Math.PI / (i + 1)));
                NotUsedColors.Remove(FoodCollection[i].FoodItemId);
            }

        }

        public static float SizeKoeff = 3;
        public static int Size;
        public static int SizeTouch;

        public static Color GetColor(byte colorIndex)
        {
            return PossibleColors[colorIndex];
        }

        public static Color[] PossibleColors = new Color[]
        {
            Color.FromArgb(255, 255, 190, 77),
            Color.FromArgb(255, 0, 182, 63),
            Color.FromArgb(255, 255, 21, 32),
            Color.FromArgb(255, 178, 25, 142),
            Color.FromArgb(255, 255, 98, 11),
            Color.FromArgb(255, 235, 61, 74),
            Color.FromArgb(255, 255, 21, 32),
            Color.FromArgb(255, 62, 69, 77),
            Color.FromArgb(255, 30, 169, 224),
            Color.FromArgb(255, 147, 211, 9),
            Color.FromArgb(255, 255, 26, 119),
            Color.FromArgb(255, 253, 197, 86),
            Color.FromArgb(255, 255, 21, 32),
            Color.FromArgb(255, 180, 216, 30),
            Color.FromArgb(255, 147, 211, 9),
            Color.FromArgb(255, 255, 27, 125),
            Color.FromArgb(255, 255, 21, 32),
            Color.FromArgb(255, 255, 21, 32),
            Color.FromArgb(255, 255, 130, 46),
            Color.FromArgb(255, 255, 224, 0),
            Color.FromArgb(255, 249, 188, 67),
            Color.FromArgb(255, 206, 0, 57),
            Color.FromArgb(255, 0, 182, 63),
            Color.FromArgb(255, 253, 195, 0),
            Color.FromArgb(255, 255, 21, 32)
        };

        public static Color GoodSnakeColor = Color.FromArgb(255, 16, 157, 89);
        public static Color BadSnakeColor = Color.FromArgb(255, 229, 102, 43);

        /*
        public static Dictionary<string, Color> SnakeColors = new Dictionary<string, Color>()
        {
            Color.FromArgb(255, 255, 190, 0),
            Color.FromArgb(255, 16, 157, 89),
            Color.FromArgb(255, 0, 140, 255),
            Color.FromArgb(255, 255, 21, 20)
        };
        */
        public static byte GetRandomColorIndex(byte withoutThis)
        {
            byte randIndex = (byte)(Utils.Rand.NextDouble() * (PossibleColors.Length - 1));

            if (randIndex == withoutThis)
                return (byte)(PossibleColors.Length - 1);
            else
                return randIndex;
        }

        public static byte GetRandomColorIndex()
        {
            byte randIndex = (byte)(Utils.Rand.NextDouble() * (NotUsedColors.Count));

            return NotUsedColors[randIndex];
        }

        public static bool IsAnyFoodNear(Vector2 position, float minDistance)
        {
            for (int i = 0; i < FoodCount; i++)
            {
                if (Vector2.Distance(position, FoodCollection[i].Position) < minDistance)
                    return true;
            }

            return false;
        }

        public static byte[] GetRandomColorIndexes(int length)
        {
            if (length <= 0 || length > PossibleColors.Length)
                return null;

            List<byte> possibelColorIndexes = new List<byte>();

            for(byte i=0; i < length; i++)
            {
                possibelColorIndexes.Add(i);
            }

            byte[] colorIndexes = new byte[length];

            for (int i = 0; i < length; i++)
            {
                colorIndexes[i] = possibelColorIndexes[(byte)(Utils.Rand.NextDouble() * (possibelColorIndexes.Count))];

                possibelColorIndexes.Remove(colorIndexes[i]);
            }

            return colorIndexes;
        }

        public static void FixIfAnyFoodIsUnaccessable()
        {
            for (int i = 0; i < Food.FoodCount; i++)
            {
                if (!AvoidObstaclesAlgorithm.CheckNotUnderApp(FoodCollection[i].Position, SizeTouch / 2) )
                {
                    FoodCollection[i].Position = NewFoodPosition();
                }
            }
        }

        public static Vector2 NewFoodPosition()
        {
            int marginToBorder = SizeTouch / 2;

            Vector2 newFoodPosition = new Vector2();

            for (int i = 0; i < 150; i++)
            {
                float newX = (float)Utils.Rand.NextDouble() * (Touch.RealWidth - 2 * marginToBorder) + marginToBorder;
                float newY = (float)Utils.Rand.NextDouble() * (Touch.RealHeight - 2 * marginToBorder) + marginToBorder;

                newFoodPosition = new Vector2(newX, newY);

                if (i < 50 && AvoidObstaclesAlgorithm.Check(newFoodPosition, SizeTouch * 2) && !Food.IsAnyFoodNear(newFoodPosition, SizeTouch * 2))
                    return newFoodPosition;

                if (i < 100 && AvoidObstaclesAlgorithm.Check(newFoodPosition, SizeTouch) && !Food.IsAnyFoodNear(newFoodPosition, SizeTouch))
                    return newFoodPosition;

                if (i >= 100 && AvoidObstaclesAlgorithm.Check(newFoodPosition, SizeTouch / 2) && !Food.IsAnyFoodNear(newFoodPosition, SizeTouch / 2))
                    return newFoodPosition;
            }

            return newFoodPosition;
        }
    }
}
