﻿using System;
using System.Numerics;

namespace Dotyk.Application.Snake
{
    public struct FoodItem
    {
        public byte FoodItemId;
        public Vector2 Position;
        //public Matrix3x2 RotateMatrix;
        public float RotateAngle;

        public static FoodItem GenerateFoodItem()
        {
            return new FoodItem(Food.GetRandomColorIndex(), Food.NewFoodPosition(), (float)(Utils.Rand.NextDouble() * 2 * Math.PI));
        }

        public FoodItem(byte foodItemId, Vector2 position, float rotateAngle)
        {
            FoodItemId = foodItemId;
            Position = position;
            RotateAngle = rotateAngle;
        }
    }
}
