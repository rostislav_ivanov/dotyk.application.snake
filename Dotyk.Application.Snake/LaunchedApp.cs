﻿using Dotyk.Shell.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public class LaunchedApp
    {
        public string Key; //A B C D
        public UILocation Location;

        public LaunchedApp(string key, UILocation location)
        {
            Key = key;
            Location = location;
        }

        public void AddAppAsObstacle()
        {
            Vector2 rectBegin = WorldConstants.ToTouchCoord(new Vector2((float)Location.Location.X, (float)Location.Location.Y));
            Vector2 rectWidthHeight = WorldConstants.ToTouchCoord(new Vector2((float)Location.Location.Width, (float)Location.Location.Height));

            AvoidObstaclesAlgorithm.AddObstacle(rectBegin, rectWidthHeight, AvoidObstaclesAlgorithm.AppObstacleColor);
        }
    }
}
