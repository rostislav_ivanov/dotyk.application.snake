﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.Graphics.DirectX;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Dotyk.Application;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI;
using System.Linq;
using Windows.Foundation;
using Microsoft.Graphics.Canvas.Brushes;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.Svg;
using Dotyk.Shell.Utils.Session;
using Dotyk.Application.Backgrounds.Client;
using Dotyk.Tech.Client;
using Dotyk.Tech.Model.Events;
using Dotyk.Tech.Model;
using Dotyk.Shell.Utils;

namespace Dotyk.Application.Snake
{
    public sealed partial class MainPage : Page
    {
        bool isTouchSimulation = false;
        bool isRelease = false;

        List<SnakeViewCircles> snakes;
        byte snakesCount = 4;

        ScaleEffect _scaleEffect;
        
        CanvasBitmap obstacalesBitmap;

        bool _isStarted;

        public MainPage()
        {
            InitializeComponent();

            DotykApplication.Current.ShellClient.Background.OnBackgroundChanged += Background_OnBackgroundChanged;
            DotykApplication.Current.ShellClient.OnApplicationStateChanged += ShellClient_OnApplicationStateChanged;
            DotykApplication.Current.ShellClient.Background.OnBackgroundChanging += Background_OnBackgroundChanging;

            this.Loaded += MainPage_Loaded;

            sliderValue1 = 100;
            sliderValue2 = 80; 
            sliderValue3 = 140;
        }

        private async void Background_OnBackgroundChanging(ShellClient source, string backgroundApp, string arguments, int x, int y, int a, long eventMutexHandler, Shell.Utils.BackgroundFile file)
        {
            if (isRelease)
            {
                StopRendering();
                await Task.Delay(20);
                await BackgroundClient.Instance.BackgroundChanging(eventMutexHandler, file);
            }
        }

        private void ShellClient_OnApplicationStateChanged(ShellClient source, Shell.Utils.Session.ApplicationState applicationState)
        {
            if (applicationState == Shell.Utils.Session.ApplicationState.Suspended)
                StopRendering();
            else
                StartRendering();
        }

        private void Background_OnBackgroundChanged(ShellClient source, string backgroundApp, string arguments)
        {
            if (!_isStarted)
            {
                StartRendering();
                _isStarted = true;
            }
            //Logger.Instance.Log($"Rendering started {App.GlobalStopwatch.Elapsed.TotalMilliseconds}");
        }

        bool paused;
        private void StopRendering()
        {
            if (!paused)
            {
                paused = true;
                canvas.Paused = true;
            }
        }

        private void StartRendering()
        {
            if (paused)
            {
                paused = false;
                canvas.Paused = false;
            }
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!isTouchSimulation)
            {
                try
                {
                    await DotykApplication.Current.ShellClient.WaitForTestServerIfAvaliable();
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception);
                }


                if (DotykApplication.Current.ShellClient.Locations.Count != 5)
                {
                    if (DotykApplication.Current.ShellClient.Locations.Select(Location => Location.Location.Y).Distinct().Count() == 2)
                    {
                        Touch.RealWidth = 189;
                        Touch.RealHeight = 64;
                        Touch.IsBar = true;
                    }
                    else
                    {
                        Touch.RealWidth = 64;
                        Touch.RealHeight = 64;
                        Touch.IsDuos = true;
                    }
                }
                /*else  //now I get 0 for rectangle table
                {
                    Touch.Width = DotykApplication.Current.ShellClient.KoSense.SensorDescription.COLUMNs;
                    Touch.Height = DotykApplication.Current.ShellClient.KoSense.SensorDescription.ROWs;
                }
                */
                BackgroundClient.Instance.Init(App.Args, downloadFile: DotykApplication.Current.ShellClient.Download);

                InitializeCanvas();

                if (isRelease)
                {
                    await Task.Delay(800);
                    StopRendering();
                    await BackgroundClient.Instance.SaveFirstFrameAsync();
                }

                unsafe
                {
                    DotykApplication.Current.ShellClient.KoSense.OnFrameReady += KoSense_OnFrameReady;
                }
                DotykApplication.Current.ShellClient.KoSense.OnKoSenseReady += KoSense_OnKoSenseReady;
                await Task.Delay(2000);
                if (!WorldConstants.TouchObj.IsInited)
                    WorldConstants.TouchObj.KoSenseInit();       
            }
            else
            {
                InitializeCanvas();
            }

            InitializeTech();
        }

        int deviceId = 0;
        DeviceMapLocation table;
        async void InitializeTech()
        {
            Dotyk.Tech.Client.Settings.dotykTechUri = "https://dotyktech-newtech.azurewebsites.net";
            Debug.Assert(!string.IsNullOrEmpty(DotykApplication.Current.ShellClient.SettingsToken), "Shell does not contain valid token. Token is null or empty.");
            await ManagementClient.Current.LoginWithDotykMe(DotykApplication.Current.ShellClient.SettingsToken);

            var myDevice = await Visitor.GetDevice();
            deviceId = myDevice.DeviceId;

            var map = await Visitor.GetMap();
            var tables = map.Devices.Values.ToList();

            if (deviceId != 0)
            {
                table = tables.FirstOrDefault(tmp => tmp.DeviceID == deviceId);
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 5);
                timer.Tick += Timer_Tick;
                timer.Start();
            }
        }

        List<LaunchedApp> apps  = new List<LaunchedApp>();
        int appsCount = 0;
        int oldAppsCount = 0;
        private async void Timer_Tick(object sender, object e)
        {
            apps = new List<LaunchedApp>();
            if (deviceId != 0 && table != null)
            {
                var _activities = await Visitor.Activities.Sync();
                if (_activities == null || !_activities.Values.Any())
                    return;
                var activity = _activities.FirstOrDefault(tmp => tmp.Value.DeviceId == deviceId);
                if (activity.Key == 0)
                    return;
                
                foreach (var place in activity.Value.Places)
                {
                    var tablePlace = table.Places[place.Key];
                    if (place.Value.Applications.Count != 2 || tablePlace.PlaceAlias == "Background")
                        continue;

                    var location = UILocation.Parse(tablePlace.Location);

                    if(location.IsFullScreen)
                        continue;

                    apps.Add(new LaunchedApp(tablePlace.PlaceAlias, location));
                }
                oldAppsCount = appsCount;
                appsCount = apps.Count;

                if (appsCount != oldAppsCount)
                    isNeedToChangeSnakesCount = true;
            }
        }

        bool isNeedToChangeSnakesCount;
        void ChangeSnakesCount()
        {
            if (!isNeedToChangeSnakesCount)
                return;

            if (!Touch.IsDuos)
            {
                if (appsCount > oldAppsCount)
                {
                    if (snakes.Count + oldAppsCount - appsCount > 1)
                    {
                        for (int i = 0; i < appsCount - oldAppsCount; i++) //1 or 2
                            snakes.RemoveAt(snakes.Count - 1);
                    }
                }
                else if (appsCount < oldAppsCount)
                {
                    if (snakes.Count + oldAppsCount - appsCount <= 4)
                    {
                        for (int i = 0; i < oldAppsCount - appsCount; i++) //1 or 2
                            snakes.Add(new SnakeViewCircles(false));
                    }
                }
            }

            isNeedToChangeSnakesCount = false;
        }

        private void AddAppsAsObstacles()
        {
            if (apps.Count == 0 || apps.Count == 4)
                return;

            foreach (var app in apps)
            {
                app.AddAppAsObstacle();
            }
        }

        private void InitializeCanvas()
        {
            Utils.Rand = new Random(DateTime.Now.Millisecond);
            AvoidObstaclesAlgorithm.ObstacleColor = isTouchSimulation ? Colors.Red : Colors.Black;
            AvoidObstaclesAlgorithm.EmptySpaceColor = isTouchSimulation ? Colors.Black : Colors.Transparent;

            AvoidObstaclesAlgorithm.Obstacles = new Color[Touch.RealWidth * Touch.RealHeight];

            for (int j = 0; j < Touch.RealWidth * Touch.RealHeight; j++)
            {
                AvoidObstaclesAlgorithm.Obstacles[j] = AvoidObstaclesAlgorithm.EmptySpaceColor;
            }

            WorldConstants.Width  = isTouchSimulation ? 1512 : (int)DotykApplication.Current.ShellClient.Locations[0].Location.Width;
            WorldConstants.Height = isTouchSimulation ? 880  : (int)DotykApplication.Current.ShellClient.Locations[0].Location.Height;
            
            WorldConstants.w_koeff = (float)WorldConstants.Width / Touch.RealWidth;
            WorldConstants.h_koeff = (float)WorldConstants.Height / Touch.RealHeight;

            snakes = new List<SnakeViewCircles>();

            SnakeModel.DefaultPositions = new Stack<SnakePartPath>();

            SnakeModel.DefaultPositions.Push(new SnakePartPath(new Vector2(Touch.RealWidth / 4, 3 * Touch.RealHeight / 4), -Math.PI / 4));
            SnakeModel.DefaultPositions.Push(new SnakePartPath(new Vector2(3 * Touch.RealWidth / 4, Touch.RealHeight / 4), 3 * Math.PI / 4));
            SnakeModel.DefaultPositions.Push(new SnakePartPath(new Vector2(3 * Touch.RealWidth / 4,  3 * Touch.RealHeight / 4), 5 * Math.PI / 4));
            SnakeModel.DefaultPositions.Push(new SnakePartPath(new Vector2(Touch.RealWidth / 4, Touch.RealHeight / 4), Math.PI / 4));

            if (Touch.IsDuos)
            {
                snakesCount = 2;
                //SnakeModel.PartDiameter = 4;
                //SnakeModel.MinMov = 3;
                Food.FoodCount = 4;
            }

            for (int i = 0; i < snakesCount; i++)
            {
                snakes.Add(new SnakeViewCircles(i == 0 ? true : false));
            }

            Food.SizeTouch = (int)(Food.SizeKoeff * snakes[snakes.Count > 1 ? 1 : 0].Model.PartDiameter  / 2);
            Food.Size = (int) (Food.SizeTouch * WorldConstants.h_koeff);
            WorldConstants.TeleportFullSize = 2 * snakes[snakes.Count > 1 ? 1 : 0].SnankeRadiusWorld;
            //WorldConstants.SnankeRadiusWorld = 88;
            //SnakeModel.DeltaMov = WorldConstants.one_div_snakeSpeed;

            Food.InitializeFoodCollection();

            canvas.CreateResources += Canvas_CreateResources;

            WorldConstants.TouchObj.KoSenseInited += TouchObj_KoSenseInited;
            WorldConstants.TouchObj.EnoughTouchesCollected += TouchObj_EnoughTouchesCollected;
        }

        void AddSnakesAsObstacles()
        {
            if (beginIsEnded)
            {
                foreach (var snake in snakes)
                {
                    snake.Model.AddItselfAsObstacle(true);
                    snake.Model.AddItselfAsObstacle(false);
                }
            }

            if (beginTime > 40)
            {
                beginIsEnded = true;
            }
            else
            {
                beginTime++;
            }
        }

        bool beginIsEnded;
        private void TouchObj_EnoughTouchesCollected()
        {
            AvoidObstaclesAlgorithm.Obstacles =
                    ((Color[])(isTouchSimulation ? AvoidObstaclesAlgorithm.TouchSimulationObstacales.Clone() :
                                                                       WorldConstants.TouchObj.ConvertTouchToColor(sliderValue1, sliderValue2, 140, true).Clone()));

            AddMenuAsObstacles();
            AddAppsAsObstacles();
            AddSnakesAsObstacles();

            obstacalesBitmap = CanvasBitmap.CreateFromColors(canvas, AvoidObstaclesAlgorithm.Obstacles, Touch.RealWidth, Touch.RealHeight);

            if (_scaleEffect != null)
                _scaleEffect.Source = obstacalesBitmap;
        }

        private void TouchObj_KoSenseInited(object sender, EventArgs e)
        {
            /*
            _scaleEffect = new Transform3DEffect
            {
                Source = obstacalesBitmap,
                TransformMatrix = WorldConstants.TouchObj._transformMatrixScreen
            };
            */
        }

        private void KoSense_OnKoSenseReady(object sender, EventArgs e)
        {
            WorldConstants.TouchObj.KoSenseInit();
        }

        private unsafe void KoSense_OnFrameReady(Dotyk.Shell.Utils.KoSense.KoPointTransformed* pPoints, Dotyk.Shell.Utils.KoSense.KoPointTransformed* pRawPoints, short maxPointCount)
        {
            WorldConstants.TouchObj.GetTouch();
        }

        void Canvas_CreateResources(CanvasAnimatedControl sender, CanvasCreateResourcesEventArgs args)
        {
            if (isTouchSimulation)
                AvoidObstaclesAlgorithm.TouchSimulationObstacales = Touch.TouchSimulation();

            
            _scaleEffect = new ScaleEffect
            {
                Source = obstacalesBitmap,
                CenterPoint = new Vector2(0.5f, 0.5f),
                Scale = new Vector2(WorldConstants.w_koeff,WorldConstants.h_koeff)
            };
            
            args.TrackAsyncAction(Canvas_CreateResourcesAsync(sender).AsAsyncAction());
        }

        private async Task Canvas_CreateResourcesAsync(CanvasAnimatedControl sender)
        {
            Food.foodBitmap = new CanvasBitmap[Food.PossibleColors.Length];
            for (int i = 0; i < Food.PossibleColors.Length; i++)
            {
                var file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(String.Format("ms-appx:///Assets/{0}.png", i + 1)));
                using (var fileStream = await file.OpenReadAsync())
                {
                    Food.foodBitmap[i] = await CanvasBitmap.LoadAsync(sender, fileStream);
                }
            }

            Food.foodBitmap_ = new CanvasBitmap[Food.PossibleColors.Length];
            for (int i = 0; i < Food.PossibleColors.Length; i++)
            {
                var file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(String.Format("ms-appx:///Assets/__{0}.png", i + 1)));
                using (var fileStream = await file.OpenReadAsync())
                {
                    Food.foodBitmap_[i] = await CanvasBitmap.LoadAsync(sender, fileStream);
                }
            }

            
            var file2 = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Shadow.png"));
            using (var fileStream = await file2.OpenReadAsync())
            {
                Food.shadowBitmap = await CanvasBitmap.LoadAsync(sender, fileStream);
            }

            Food.foodShadowBitmap = new CanvasBitmap[Food.PossibleColors.Length];
            for (int i = 0; i < Food.PossibleColors.Length; i++)
            {
                var file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(String.Format("ms-appx:///Assets/_{0}.png", i + 1)));
                using (var fileStream = await file.OpenReadAsync())
                {
                    Food.foodShadowBitmap[i] = await CanvasBitmap.LoadAsync(sender, fileStream);
                }
            }

            canvas.Draw += Canvas_Draw;
            canvas.Update += Canvas_Update;
        }

        void AddMenuAsObstacles()
        {
            if (Touch.IsDuos)
            {
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(4, Touch.RealHeight / 2 + 2), 3, AvoidObstaclesAlgorithm.AppObstacleColor);
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(Touch.RealWidth - 4, Touch.RealHeight / 2 - 2), 3, AvoidObstaclesAlgorithm.AppObstacleColor);
            }
            else
            {
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(5, Touch.RealHeight / 2 - 5), 4, AvoidObstaclesAlgorithm.AppObstacleColor);
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(5, Touch.RealHeight / 2 + 5), 4, AvoidObstaclesAlgorithm.AppObstacleColor);
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(Touch.RealWidth - 5, Touch.RealHeight / 2 - 5), 4, AvoidObstaclesAlgorithm.AppObstacleColor);
                AvoidObstaclesAlgorithm.AddObstacle(new Vector2(Touch.RealWidth - 5, Touch.RealHeight / 2 + 5), 4, AvoidObstaclesAlgorithm.AppObstacleColor);
            }
        }

        int beginTime = 0;
        int generalOldDrawIndex = -1, generalDrawIndex = 0;

        void Canvas_Update(ICanvasAnimatedControl sender, CanvasAnimatedUpdateEventArgs args)
        {
            if (generalOldDrawIndex == generalDrawIndex)
                return;

            generalOldDrawIndex = generalDrawIndex;

            //AddSnakesAsObstacles();
            if (generalOldDrawIndex % 20 == 0)
            {
                Food.FixIfAnyFoodIsUnaccessable();
                ChangeSnakesCount();
            }

            foreach (var snake in snakes)
            {
                snake.UpdateModelAndView(sender);
            }
        }

        void Canvas_Draw(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args)
        {
            //if ((isTouchSimulation || !isRelease) && _scaleEffect != null && _scaleEffect.Source != null)
            //    args.DrawingSession.DrawImage(_scaleEffect);

            DrawFood(args.DrawingSession);

            foreach (var snake in snakes)
            {
                snake.DrawSnake(args.DrawingSession);
            }

            generalDrawIndex++;
        }

        void DrawFood(CanvasDrawingSession session)
        {
            foreach (FoodItem foodItem in Food.FoodCollection)
                DrawFoodItem(session, foodItem);
        }

        void DrawFoodItem(CanvasDrawingSession session, FoodItem foodItem, bool addGlow = false)
        {
            CanvasBitmap tmpBitmap = Food.foodShadowBitmap[foodItem.FoodItemId];
            session.DrawImage(tmpBitmap,
                        new Windows.Foundation.Rect(WorldConstants.ToWorldCoord(foodItem.Position).X - Food.Size / 2f,
                                          WorldConstants.ToWorldCoord(foodItem.Position).Y - Food.Size / 2f,
                                          Food.Size,
                                          Food.Size),
                        new Windows.Foundation.Rect(0, 0, tmpBitmap.Size.Width,
                                                          tmpBitmap.Size.Height),
                         1.0f,
                                 CanvasImageInterpolation.Anisotropic,
                                 Matrix4x4.CreateTranslation(-WorldConstants.ToWorldCoord(foodItem.Position).X, -WorldConstants.ToWorldCoord(foodItem.Position).Y, 0) *
                                 Matrix4x4.CreateRotationZ(foodItem.RotateAngle) *
                                 Matrix4x4.CreateTranslation(WorldConstants.ToWorldCoord(foodItem.Position).X, WorldConstants.ToWorldCoord(foodItem.Position).Y, 0));

            tmpBitmap = Food.foodBitmap[foodItem.FoodItemId];
            session.DrawImage(tmpBitmap,
                                 new Windows.Foundation.Rect(WorldConstants.ToWorldCoord(foodItem.Position).X - Food.Size  / 2f,
                                          WorldConstants.ToWorldCoord(foodItem.Position).Y - Food.Size / 2f,
                                          Food.Size,
                                          Food.Size),
                                 new Windows.Foundation.Rect(0,0, tmpBitmap.Size.Width, tmpBitmap.Size.Height), 
                                 1.0f,
                                 CanvasImageInterpolation.Anisotropic,
                                 Matrix4x4.CreateTranslation(-WorldConstants.ToWorldCoord(foodItem.Position).X, -WorldConstants.ToWorldCoord(foodItem.Position).Y, 0) *
                                 Matrix4x4.CreateRotationZ(foodItem.RotateAngle) *
                                 Matrix4x4.CreateTranslation( WorldConstants.ToWorldCoord(foodItem.Position).X,  WorldConstants.ToWorldCoord(foodItem.Position).Y, 0));
        }


        #region Code for debug

        Random rand = new Random();
        int issdsd = 0;
        bool testBool = false;
        bool testBool1 = true;
        bool testBool2 = false;

        int sliderValue1, sliderValue2, sliderValue3;
        private void Slider1_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            //sliderValue1 = (int)Slider1.Value;
        }

        private void Slider2_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            //sliderValue2 = (int)Slider2.Value;
        }

        private void Slider4_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            //sliderValue3 = (int)Slider4.Value;
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (paused)
                StartRendering();
            else
                StopRendering();
            
        }
        /*

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            testBool = !testBool;
            //issdsd++;
            //textBlockBlend.Text = testBool + " " + testBool1 + " " + testBool2;
            //await surfaceToEffects2.SaveAsync("Assets/specularEffect.png", CanvasBitmapFileFormat.Png);   (BlendEffectMode)

            //string filename = "touch " + rand.Next() + ".png";
            //StorageFolder pictureFolder = KnownFolders.SavedPictures;
            //var file = await pictureFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
            //using (var fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
            //{
            //    await obstacalesBitmap.SaveAsync(fileStream, CanvasBitmapFileFormat.Png, 1f);
            //}

        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            testBool2 = !testBool2;

            var sessions = await DotykApplication.Current.ShellClient.Session.GetSessions();

            string sefsfsfe = "\r\n";

            if (sessions != null)
            {
                foreach (var session in sessions)
                {
                    sefsfsfe += session.PlaceLocation.Location.X + " " + session.PlaceLocation.Location.Y + " " +
                                session.PlaceLocation.Location.Width + " " + session.PlaceLocation.Location.Height + "\r\n";
                }
            }
            else
                sefsfsfe = "NULL";

            textBlockBlend.Text = testBool + " " + testBool1 + " " + testBool2 + sefsfsfe;
        }

        float sliderValue1, sliderValue2, sliderValue3;
        private void Slider1_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (snake1.CurrentSnake == null)
                return;

            snake1.CurrentSnake.Color = Color.FromArgb(255, (byte)Slider1.Value, snake1.CurrentSnake.Color.G, snake1.CurrentSnake.Color.B);

            textBlockBlend.Text = Slider1.Value + " " + Slider2.Value + " " + Slider3.Value;

            //sliderValue1 = (float)Slider1.Value;
            //snake = new SnakeBody((int)Slider1.Value);
            //avoidObstaclesAlgorithm = new AvoidObstaclesAlgorithm(snake, Touch.Width, Touch.Height, 10);
        }

        private void Slider2_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (snake1.CurrentSnake == null)
                return;

            snake1.CurrentSnake.Color = Color.FromArgb(255, snake1.CurrentSnake.Color.R, (byte)Slider2.Value, snake1.CurrentSnake.Color.B);

            textBlockBlend.Text = Slider1.Value + " " + Slider2.Value + " " + Slider3.Value;
            //sliderValue2 = (float)Slider2.Value;
        }

        private void Slider3_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (snake1.CurrentSnake == null)
                return;

            snake1.CurrentSnake.Color = Color.FromArgb(255, snake1.CurrentSnake.Color.R, snake1.CurrentSnake.Color.G, (byte)Slider3.Value);

            textBlockBlend.Text = Slider1.Value + " " + Slider2.Value + " " + Slider3.Value;
            //sliderValue3 = (int)Slider3.Value;

            //textBlockBlend.Text = ((BlendEffectMode)sliderValue3).ToString();
        }



        */

#endregion

        }
}
