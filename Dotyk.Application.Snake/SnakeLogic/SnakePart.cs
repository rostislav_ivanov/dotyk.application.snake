﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Dotyk.Application.Snake
{
    public class SnakePart
    {
        public SnakePartPath CurrPos;
        public LinkedList<Vector2> Path;
        public byte ColorIndex;

        public byte? SwallowedFoodColorIndex;
        public int FoodStuckedAnimationEncounter = -1; //ToDo: move it from here

        public bool ToDelete = false;

        public SnakePart(SnakePartPath defPosition, byte color, bool useThisColorOrExceptThisColor)
        {
            CurrPos = defPosition;

            Path = new LinkedList<Vector2>();

            ColorIndex = useThisColorOrExceptThisColor ? color : Food.GetRandomColorIndex(color);
        }

        public Vector2 GetNextPosition()
        {
            if (Path.First != null)
            {
                CurrPos.Position = Path.First.Value;
                Path.RemoveFirst();
            }

            return CurrPos.Position;
        }
    }
}
