﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Snake
{
    public class SnakePartPath  //ToDo: maybe it is better to make as a struct; maybe it is better to divide on two classes: Position+Angle & Angle+Distance
    {
        public Vector2 Position;
        public double Angle;

        public float Dist = 0;
        public float LeftDist;

        private float horda;
        private double hordaAngle;
        private bool isMovementLinear = false;

        private static double maxArcAngle = Math.PI / 4;

        public int AngleIndex = -1;
        public double AngleDelta;
        public bool Condition = false;

        public SnakePartPath()
        {
        }

        public SnakePartPath(Vector2 position, float dist, double angle)
        {
            Position = position;
            Angle = angle;

            Dist = dist;
            LeftDist = dist;
        }

        public SnakePartPath(SnakePartPath headPath, float dist)
        {
            Position = headPath.Position;
            Angle = headPath.Angle;

            Dist = dist;
            LeftDist = dist;
        }

        //constructor for the head
        public SnakePartPath(Vector2 position, double angle)
        {
            Position = position;
            Angle = angle;
        }

        public SnakePartPath(float dist, double angle)
        {
            Dist = dist;
            LeftDist = dist;
            Angle = angle;
        }

        public SnakePartPath(double angle)
        {
            Angle = angle;
        }

        public Vector2 CalculateDeltaMovToNextPos(SnakePartPath nextPos, bool? direction = null)
        {
            double newAngle;
            if (isMovementLinear)
            {
                Vector2 delete = new Vector2(horda * (float)Math.Cos(hordaAngle),
                                             horda * (float)Math.Sin(hordaAngle));

                newAngle = Angle;
                return delete;
            }
            else
            {
                int current_step;
                /*if (Angle > nextPos.Angle)
                {
                    current_step = (int)((nextPos.LeftDist) / SnakeBody.DeltaMov);
                }
                else*/
                {
                    current_step = (int)((nextPos.Dist - nextPos.LeftDist) / horda);
                }
                //newAngle = current_step * hordaAngle + Angle;               //right arc (current_step - 0.5)

                int n = (int)(nextPos.Dist / horda);

                double koef = (n - Math.Sqrt(n * n - current_step * current_step));

                newAngle = koef * hordaAngle + Angle;

                Vector2 delete = new Vector2(horda * (float)Math.Cos(newAngle),
                                             horda * (float)Math.Sin(newAngle));

                //newAngle = 0.5 * current_step * hordaAngle + Angle; //ToDo
                return delete;
            }
        }

        //float deltaMov;
        public void InitializeMovement(SnakePartPath nextPos, float deltaMov)
        {
            int stepsCount = (int)(nextPos.Dist / deltaMov);
            //double distance = Vector2.Distance(nextPos.Position, Position);

            hordaAngle = nextPos.Angle - Angle;

            if (hordaAngle == 0) //movement is linear
            {
                isMovementLinear = true;
                horda = deltaMov;
                //horda = (float) (distance / stepsCount);
                hordaAngle = nextPos.Angle;
                return;
            }

            isMovementLinear = false;
            //double newCurrPosAngle = Angle;
            if (Math.Abs(hordaAngle) > maxArcAngle)
            {
                do
                {
                    Angle += nextPos.Angle > Angle ? maxArcAngle : -maxArcAngle;
                }
                while (Math.Abs(nextPos.Angle - Angle) > maxArcAngle);
                hordaAngle = nextPos.Angle - Angle;
            }

            //float radius = (float)Math.Abs(0.5 * distance / Math.Sin(hordaAngle));


            hordaAngle = hordaAngle / stepsCount;  //right arc 2 * hordaAngle / stepsCount

            //horda = (float)Math.Abs(Math.Sqrt(2) * radius * Math.Sqrt(1 - Math.Cos(hordaAngle)));
            horda = deltaMov;
        }
    }
}
