﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public class SnakeModel
    {
        public LinkedList<SnakePart> Body;
        public List<Vector2> HeadPath;
        public int Length;                  //ToDo: remove
        public int PartDiameter;
        public float DistBetweenParts;

        public bool IsTeleporting = false;

        public Vector2 TeleportEnterPos;
        public Vector2 TeleportExitPos;

        public float MinMov;
        private float deltaMov;

        private List<LinkedListNode<SnakePart>> swallowedFoodPositions;

        public Queue<byte> ColorsForTeleportedSnake;
        public int DeathAnimationEncounter = -1;
        private SnakePart beginOfTailToDelete;

        public int DefaultLength;

        public int MaxLength
        {
            get
            {
                return Food.PossibleColors.Length + MinLength;
            }
        }

        public int MinLength
        {
            get
            {
                return IsGoodSnake ? 5 : 10; // DefaultLength / 2;
            }
        }

        public int IncreaseLengthAfterFood
        {
            get
            {
                return 1; // DefaultLength / 10;
            }
        }

        public int DecreaseLengthAfterTeleport
        {
            get
            {
                return -1; //-DefaultLength / 10;
            }
        }

        //public int MaxSizeAfterTeleport;

        public Color ObstacleColorForOther;
        public Color ObstacleColorForMyself;

        public int CountOfBodyPartsToNextTanged
        {
            get
            {
                return (int)(PartDiameter / DistBetweenParts);
            }
        }

        public int CountOfBodyPartsToNextHeadPositions
        {
            get
            {
                return (int)(DistBetweenParts / deltaMov);
            }
        }

        public Vector2 HeadPosition
        {
            get
            {
                return Body.First.Value.CurrPos.Position;
            }
        }

        public int Radius
        {
            get
            {
                return (int)Math.Round(PartDiameter / 2.0);
            }
        }

        public static Stack<SnakePartPath> DefaultPositions;

        public static int PathLength //frames count
        {
            get
            {
                return 12; // (int)(MinMov / DeltaMov);
            }
        }


        public bool IsGoodSnake;
        public SnakeModel(bool _isSnakeSmooth, bool isSnakeGood, int length = 0)
        {
            PartDiameter = 4;
            MinMov = isSnakeGood ? 3f : 2.5f;
            deltaMov = isSnakeGood ? 3f / 12f : 2.5f / 12f;
            DistBetweenParts = MinMov;//isSnakeSmooth ? 0.45f : 4;
            //DefaultLength = isSnakeSmooth ? 12 * PartDiameter : (int)(5 + Utils.Rand.NextDouble() * 10);
             DefaultLength = isSnakeGood ? 12 : 10;

            Length = length == 0 ? DefaultLength : length;
            //ToDo: replace random to formula
            ObstacleColorForOther =  Color.FromArgb(255, (byte)(Utils.Rand.NextDouble() * 255), (byte)(Utils.Rand.NextDouble() * 255), (byte)(Utils.Rand.NextDouble() * 255));
            ObstacleColorForMyself = Color.FromArgb(255, (byte)(Utils.Rand.NextDouble() * 255), (byte)(Utils.Rand.NextDouble() * 255), (byte)(Utils.Rand.NextDouble() * 255));

            HeadPath = new List<Vector2>();
            swallowedFoodPositions = new List<LinkedListNode<SnakePart>>();

            IsGoodSnake = isSnakeGood;
        }

        public void InitializeSnakePos(SnakePartPath firthPath)
        {
            Body = new LinkedList<SnakePart>();

            if (Length == 0) Length = MinLength;

            byte[] colorIndexes = Food.GetRandomColorIndexes(Length - MinLength);

            for (int i = 0; i < Length; i++)
            {
                Body.AddLast(new SnakePart(new SnakePartPath(firthPath.Position, firthPath.Angle), i < MinLength ? byte.MaxValue : colorIndexes[i - MinLength], true));
            }

            //if (!isSnakeSmooth)
           //     Food.NotUsedColors.Remove(Body.First.Value.ColorIndex);
        }

        #region Shaders
        /*
        public Color[] RenderSnake(double worldCoordKoeffWidth, double worldCoordKoeffHeight)
        {
            Color[] bytesForsnakePartsBitmap = new Color[Length];
            int partIndex = 0;
            foreach (var part in Body)
            {
                uint pXWC = (uint)(part.CurrPos.X * worldCoordKoeffWidth);  //part.X in world coordinates
                uint pYWC = (uint)(part.CurrPos.Y * worldCoordKoeffHeight); //part.Y in world coordinates

                bytesForsnakePartsBitmap[partIndex] = Color.FromArgb((byte)(((pXWC) >> 8) & 0xFF),
                (byte)(((pXWC) >> 0) & 0xFF), (byte)(((pYWC) >> 8) & 0xFF), (byte)(((pYWC) >> 0) & 0xFF));

                partIndex++;
            }

            return bytesForsnakePartsBitmap;
        }
        */
        #endregion Shaders

        public SnakePartPath AngleToNewHeadPos(double angle, SnakePartPath possibleHeadPos = null, List<Vector2> path = null, bool? direction = null)
        {
            if (Body == null || Body.Count == 0)
                return null;

            
            if (angle > 2 * Math.PI)
                angle -= 2 * Math.PI;
            else if (angle < -2 * Math.PI)
                angle += 2 * Math.PI;
                
            SnakePartPath currPos;
            if (possibleHeadPos != null)
            {
                currPos = new SnakePartPath(possibleHeadPos.Position, possibleHeadPos.Dist, possibleHeadPos.Angle);
            }
            else
            {
                currPos = Body.First.Value.CurrPos;
            }


            SnakePartPath nextPos = new SnakePartPath(currPos.Position, MinMov, angle);

            currPos.InitializeMovement(nextPos, deltaMov);
            while (nextPos.LeftDist >= deltaMov)
            {
                nextPos.LeftDist -= deltaMov;
                nextPos.Position += currPos.CalculateDeltaMovToNextPos(nextPos, direction);

                if (path != null)
                    path.Add(nextPos.Position);
            }

            return nextPos;
        }

        public SnakePartPath AngleToNewHeadPosEstimation(double angle, double move)
        {
            if (Body == null || Body.Count == 0)
                return null;

            return new SnakePartPath(new Vector2((float)(HeadPosition.X + move * Math.Cos(angle)), (float)(HeadPosition.Y + move * Math.Sin(angle))), angle);

        }

        public void MoveSnake(SnakePartPath NewHeadPos, bool isTeleport)
        {
            AngleToNewHeadPos(NewHeadPos.Angle, isTeleport ? NewHeadPos : null, HeadPath);
            //segment
            //SmoothPath = Segment.CalculateSpline(HeadPath);

            Body.First.Value.CurrPos.Angle = NewHeadPos.Angle;

            FollowHead();
        }


        //if only MinMov > DistBetweenParts
        void FollowHead()
        {
            LinkedListNode<SnakePart> currSnakePart = Body.First;

            int pathBeginIndex = HeadPath.Count - PathLength;

            while (currSnakePart != null)
            {
                for (int i = 0; i < PathLength; i++)
                {
                    if (pathBeginIndex + i == HeadPath.Count || pathBeginIndex + i < 0)
                        return;

                    currSnakePart.Value.Path.AddLast(HeadPath[pathBeginIndex + i]);
                }

                pathBeginIndex -= (int)(DistBetweenParts / deltaMov);
                currSnakePart = currSnakePart.Next;
            }

            //if path longer than snake - remove unnecessary end of path 
            if (pathBeginIndex > 0)
            {
                HeadPath.RemoveRange(0, pathBeginIndex);
            }
        }

       
        public void Stop()
        {
            LinkedListNode<SnakePart> currSnakePart = Body.First;

            //int pathBeginIndex = HeadPath.Count - PathLength;

            int countOfWrongPathSteps = currSnakePart.Value.Path.Count;

            while (currSnakePart != null)
            {
                currSnakePart.Value.Path = new LinkedList<Vector2>();
                /*
                for (int i = 0; i < PathLength; i++)
                {
                    if (pathBeginIndex + i == HeadPath.Count || pathBeginIndex + i < 0)
                        return;

                    currSnakePart.Value.Path.AddLast(HeadPath[pathBeginIndex + i]);
                }*/

                //pathBeginIndex -= (int)(DistBetweenParts / deltaMov);
                currSnakePart = currSnakePart.Next;
            }

            HeadPath.RemoveRange(HeadPath.Count - countOfWrongPathSteps, countOfWrongPathSteps);
            //HeadPath.RemoveRange(0, countOfWrongPathSteps);
        }

        public MinMaxRegion GetWindow()
        {
            MinMaxRegion window = new MinMaxRegion(new Vector2(float.MaxValue, float.MaxValue), new Vector2(float.MinValue, float.MinValue));

            //AddTeleportToWindow(ref window);

            if (Body == null || Body.Count == 0)
                return window;

            LinkedListNode<SnakePart> bodyPart = Body.First;

            while (bodyPart.Next != null)
            {
                bodyPart = bodyPart.Next;

                AddPointToWindow(ref window, bodyPart.Value.Path.Count == 0 ? bodyPart.Value.CurrPos.Position : bodyPart.Value.Path.Last.Value);
            }

            bodyPart = Body.Last;
            AddPointToWindow(ref window, bodyPart.Value.CurrPos.Position);

            bodyPart = Body.First;
            AddPointToWindow(ref window, bodyPart.Value.Path.Count == 0 ? bodyPart.Value.CurrPos.Position : bodyPart.Value.Path.Last.Value);

            //if(HeadPath.Count > 0)
            //    AddPointToWindow(HeadPath[0]);


            return window;
        }

        private void AddPointToWindow(ref MinMaxRegion window, Vector2 point)
        {
            if (point.X < window.MinPoint.X)
                window.MinPoint.X = point.X;

            if (point.Y < window.MinPoint.Y)
                window.MinPoint.Y = point.Y;

            if (point.X > window.MaxPoint.X)
                window.MaxPoint.X = point.X;

            if (point.Y > window.MaxPoint.Y)
                window.MaxPoint.Y = point.Y;
        }

        public void ChangeSizeAfterTeleport() // -1 or 1
        {
            if(IsGoodSnake)
                ChangeSize(DecreaseLengthAfterTeleport);
        }

        public void ChangeSizeToMin()
        {
            ChangeSize(MinLength - Body.Count);
        }

        public void ChangeSize(int growDelta, byte? colorIndex = null)
        {
            if (Body.Count == 0)
                return;

            //if (Body.Count + growDelta < 0 && isTeleport)
            //    growDelta = -Body.Count;

            if (Body.Count + growDelta < MinLength)
                growDelta = MinLength - Body.Count;

            if (Body.Count + growDelta > MaxLength)
                growDelta = (MinLength - Body.Count);

            //byte[] colorIndexes = Food.GetRandomColorIndexes(Length); for change size to min

            if (growDelta < 0)
            {
                var snakePartToDelete = Body.Last;
                for (int i = 0; i < Math.Abs(growDelta); i++)
                    snakePartToDelete = snakePartToDelete.Previous;

                MarkToDeleteAfterFoodOvereaten(snakePartToDelete.Value);
            }
            else
            {
                for (int i = 0; i < Math.Abs(growDelta); i++)
                {
                    byte tmpColorIndex = 0;
                    bool useThisColorOrExceptThisColor = true;

                    if (Body.Count < MinLength)
                    {
                        tmpColorIndex = byte.MaxValue;
                    }
                    else if (colorIndex != null)
                    {
                        tmpColorIndex = (byte)colorIndex;
                    }
                    else
                    {
                        tmpColorIndex = Body.Last.Value.ColorIndex;
                        useThisColorOrExceptThisColor = false;
                    }

                    Body.AddLast(new SnakePart(
                                    new SnakePartPath(Body.Last.Value.Path.Count > 0 ? Body.Last.Value.Path.Last.Value : Body.Last.Value.CurrPos.Position, deltaMov),
                                    tmpColorIndex, useThisColorOrExceptThisColor));

                    if (IsGoodSnake)
                        GrowDiameter();
                }
            }
        }

        public void MarkToDeleteAfterFoodOvereaten(SnakePart snakePartWithStuckedFood)
        {
            if (snakePartWithStuckedFood.ToDelete)
                return;
            
            DeathAnimationEncounter = 80;
            beginOfTailToDelete = snakePartWithStuckedFood;

            var snakePart = Body.Last;
            while (snakePart == null || snakePartWithStuckedFood != snakePart.Value)
            {
                snakePart.Value.ToDelete = true;
                snakePart = snakePart.Previous;
            }
        }

        public void ChangeSizeAfterFoodOvereaten()
        {
            if (!IsGoodSnake)
                return;

            if (Body.Contains(beginOfTailToDelete))
            {
                while (beginOfTailToDelete != Body.Last.Value)
                {
                    Body.RemoveLast();
                }
            }
            else
            {

            }

            DeathAnimationEncounter = -1;
            beginOfTailToDelete = null;

            if (IsGoodSnake)
                GrowDiameter();
        }

        public void ChangeSizeRemoveWhatIsNoTeleported()
        {
            var bodyPart = Body.Last;

            while (Body.Count > 1 && Vector2.Distance(bodyPart.Value.CurrPos.Position, TeleportExitPos) > PartDiameter * 4)
            {
                bodyPart = bodyPart.Previous;
                Body.RemoveLast();
            }

            if (Body.Count < MinLength)
            {
                ChangeSizeToMin();
            }

            if (IsGoodSnake)
                GrowDiameter();
        }

        public void GrowDiameter()
        {
           
            if (Body.Count <= 18)
                PartDiameter = 4;
            else if (Body.Count > 18)
                PartDiameter = 5;
                
        }

        public void AddItselfAsObstacle(bool isForOtherSnakes = false)
        {
            if (Body == null || Body.Count == 0)
            {
                return;
            }

            int r = isForOtherSnakes ? Radius + 1 : Radius - 1;
            
            LinkedListNode<SnakePart> bodyPart = Body.First.Next;
            /*
            while(bodyPart != null)
            {
                AddObstacle(bodyPart.Value.CurrPos.Position, r, isForOtherSnakes);

                bodyPart = bodyPart.Next;
            }
            */
            LinkedListNode<SnakePart> previousPart = Body.First;
            
            if(isForOtherSnakes)
                AddObstacle(Body.First.Value.CurrPos.Position, r, isForOtherSnakes);

            int k = isForOtherSnakes ? -1 : 0;
            while (k < Body.Count - CountOfBodyPartsToNextTanged && bodyPart.Next != null)
            {
                bodyPart = bodyPart.Next;
                k++;

                if (k % CountOfBodyPartsToNextTanged != 0)
                    continue;

                if (!isForOtherSnakes && Vector2.Distance(bodyPart.Value.CurrPos.Position, previousPart.Value.CurrPos.Position) < Radius+1)
                {
                    continue;
                }

                previousPart = bodyPart;

                AddObstacle(bodyPart.Value.CurrPos.Position, r, isForOtherSnakes);
            }
            
        }

        //move to AvoidObstaclesAlgorithm
        public void AddObstacle(Vector2 position, int r, bool isForOtherSnakes)
        {
            int pX = (int)Math.Round(position.X);
            int pY = (int)Math.Round(position.Y);

            int npX, npY;

            for (int i = -r; i <= r; i++)
            {
                for (int j = -r; j <= r; j++)
                {
                    if (i * i + j * j <= r * r)
                    {
                        npX = pX + j;
                        npY = pY + i;

                        if (npY < Touch.RealHeight && npX < Touch.RealWidth && npX >= 0 && npY >= 0 && AvoidObstaclesAlgorithm.Obstacles[npY * Touch.RealWidth + (pX + j)] != AvoidObstaclesAlgorithm.ObstacleColor)
                            AvoidObstaclesAlgorithm.Obstacles[npY * Touch.RealWidth + (pX + j)] = isForOtherSnakes ? ObstacleColorForOther : ObstacleColorForMyself;
                    }
                }
            }
        }

        public void SwallowFood(byte foodColor)
        {
            if (Body != null && Body.Count > 1)
            {
                Body.First.Next.Value.SwallowedFoodColorIndex = (byte)foodColor;
                swallowedFoodPositions.Add(Body.First.Next);
            }
        }

        public void MoveFood()
        {
            try
            {
                byte tmpColorIndex;

                for (int i = 0; i < swallowedFoodPositions.Count; i++)
                {
                    if (swallowedFoodPositions[i].Value.SwallowedFoodColorIndex == swallowedFoodPositions[i].Value.ColorIndex)
                    {
                        swallowedFoodPositions[i].Value.FoodStuckedAnimationEncounter = 100;
                        MarkToDeleteAfterFoodOvereaten(swallowedFoodPositions[i].Value);
                        swallowedFoodPositions.RemoveAt(i);
                        i--;
                        continue;
                    }

                    tmpColorIndex = (byte)swallowedFoodPositions[i].Value.SwallowedFoodColorIndex;
                    swallowedFoodPositions[i].Value.SwallowedFoodColorIndex = null;

                    if (swallowedFoodPositions[i].Next != null && swallowedFoodPositions[i].Next.Value.SwallowedFoodColorIndex == null)
                    {
                        swallowedFoodPositions[i] = swallowedFoodPositions[i].Next;
                        swallowedFoodPositions[i].Value.SwallowedFoodColorIndex = tmpColorIndex;
                    }
                    else
                    {
                        if (IsGoodSnake)
                            ChangeSize(IncreaseLengthAfterFood, tmpColorIndex);
                        swallowedFoodPositions.RemoveAt(i);
                        i--;
                    }
                }
            }
            catch(Exception e)
            {

            }
        }

        public void BackupColorsBeforeTeleport()
        {
            if (Body == null || Body.Count <= MinLength)
                return;

            ColorsForTeleportedSnake = new Queue<byte>();

            LinkedListNode<SnakePart> bodyPart = Body.First.Next.Next.Next.Next.Next;

            while(bodyPart != null)
            {
                ColorsForTeleportedSnake.Enqueue(bodyPart.Value.ColorIndex);
                bodyPart = bodyPart.Next;
            }
        }
    }
}
