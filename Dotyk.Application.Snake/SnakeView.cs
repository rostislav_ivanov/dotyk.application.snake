﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Brushes;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public abstract class SnakeView
    {
        public SnakeModel Model;
        public AvoidObstaclesAlgorithm avoidObstaclesAlgorithm;

        public double OldAngle;
        public double NewAngle = Double.NaN;

        public SnakePartPath NewHeadPos;
        private Vector2 head_pos;

        public int drawIndex = 0;
        public byte updateIndex = 0;

        public float SnankeRadiusWorld
        {
            get
            {
                return Model.PartDiameter * WorldConstants.h_koeff / 2;
            }
        }
        private float teleportRadius;

        public Color Color;
        protected Color NewColor;

        bool touchedOpacitySign;
        byte touchedOpacity = 180;

        private int obstaclesFrontHeadSize
        {
            get
            {
                return Model.PartDiameter * 4;
            }
        }

        public SnakeView(bool isSnakeSmooth, bool isGoodSnake, LinkedList<SnakePart> newBody = null)
        {
            if (null == newBody)
            {
                Model = new SnakeModel(isSnakeSmooth, isGoodSnake);
                if(SnakeModel.DefaultPositions.Count > 0)
                    Model.InitializeSnakePos(SnakeModel.DefaultPositions.Pop());
                else
                    Model.InitializeSnakePos(new SnakePartPath(Food.NewFoodPosition(), Utils.Rand.NextDouble() * 360));
            }
            else
            {
                Model = new SnakeModel(isSnakeSmooth, isGoodSnake, newBody.Count - 1);
                Model.Body = new LinkedList<SnakePart>(newBody.Skip(1));
            }

            avoidObstaclesAlgorithm = new AvoidObstaclesAlgorithm(Model, Touch.RealWidth, Touch.RealHeight, 40);

            Color = isGoodSnake ? Food.GoodSnakeColor : Food.BadSnakeColor;
            teleportRadius = SnankeRadiusWorld;
        }

        byte? foodEatenColor = null;
        public void UpdateModelAndView(ICanvasAnimatedControl canvas)
        {
            if (drawIndex != 0 && drawIndex % 3 != 1)
                return;

            if (drawIndex != 0)
            {
                if (avoidObstaclesAlgorithm.CheckIfTouchedStateIsChanged() ||
                    avoidObstaclesAlgorithm.CheckCurrentRoute())
                    Stop();
                else
                    return;
            }

            foodEatenColor = null;
            if (NewHeadPos != null)
            {
                for (int i = 0; i < Food.FoodCount; i++)
                {
                    if (Vector2.Distance(NewHeadPos.Position, Food.FoodCollection[i].Position) < Model.PartDiameter * 1.5)
                    {
                        foodEatenColor = Food.FoodCollection[i].FoodItemId;
                        Food.NotUsedColors.Add(Food.FoodCollection[i].FoodItemId);
                        Food.FoodCollection[i] = FoodItem.GenerateFoodItem();
                        Food.NotUsedColors.Remove(Food.FoodCollection[i].FoodItemId);
                    }
                }
            }

            OldAngle = NewAngle;

            NewHeadPos = avoidObstaclesAlgorithm.Run(OldAngle);

            if (NewHeadPos.Angle >  WorldConstants.maxAngle)
                NewHeadPos.Angle -= WorldConstants.maxAngle;

            if (NewHeadPos.Angle < -WorldConstants.maxAngle)
                NewHeadPos.Angle += WorldConstants.maxAngle;

            NewAngle = NewHeadPos.Angle;

            Model.AddObstacle(NewHeadPos.Position, Model.Radius, true);

            UpdateView(canvas);
        }

        public void UpdateView(ICanvasAnimatedControl canvas)
        {
            if (avoidObstaclesAlgorithm.State != SnakeState.Touched && avoidObstaclesAlgorithm.State != SnakeState.Stucked)
            {
                Model.MoveSnake(NewHeadPos, (avoidObstaclesAlgorithm.State == SnakeState.TeleportStarted) ? true : false);
            }

            UpdateViewColors(foodEatenColor);

            /*
            if (avoidObstaclesAlgorithm.State == SnakeState.Released)
            {
                Model.ChangeSizeAfterTeleport();
            }
            */

            if (avoidObstaclesAlgorithm.State == SnakeState.TeleportStarted)
            {
                teleportRadius = SnankeRadiusWorld;
            }

            drawIndex = -1;

            updateIndex++;
        }

        protected abstract void UpdateViewColors(byte? foodEatenColor);

        public void DrawSnake(CanvasDrawingSession session)
        {
            if (drawIndex == 0)
                return;

            if (drawIndex == -1)
                drawIndex = 1;
            else
                drawIndex++;

            if (Model.Body == null)
                    return;

            SwallowFood(updateIndex * SnakeModel.PathLength + drawIndex);

            LinkedListNode<SnakePart> snakePart;

            if (AvoidObstaclesAlgorithm.IsTeleporting(avoidObstaclesAlgorithm.State))
            {
                if(avoidObstaclesAlgorithm.State == SnakeState.TeleportStarted && teleportRadius < WorldConstants.TeleportFullSize)
                {
                    teleportRadius += (WorldConstants.TeleportFullSize - SnankeRadiusWorld) / ( SnakeModel.PathLength);
                }

                if (teleportRadius > 0 && teleportRadius < WorldConstants.TeleportFullSize)
                {
                    teleportRadius += (WorldConstants.TeleportFullSize - SnankeRadiusWorld) / ( SnakeModel.PathLength);
                }

                if (avoidObstaclesAlgorithm.State == SnakeState.TeleportFinished)
                {
                    teleportRadius -= WorldConstants.TeleportFullSize / ( SnakeModel.PathLength);
                }

                session.FillCircle(WorldConstants.ToWorldCoord(Model.TeleportEnterPos), teleportRadius, new CanvasRadialGradientBrush(session.Device,
                    new CanvasGradientStop[2] { new CanvasGradientStop() { Color = Colors.Blue, Position = 0 }, new CanvasGradientStop() { Color = Colors.Black, Position = 1 } },
                    CanvasEdgeBehavior.Clamp,
                    CanvasAlphaMode.Premultiplied)
                {
                    RadiusX = teleportRadius,
                    RadiusY = teleportRadius,
                    Center = WorldConstants.ToWorldCoord(Model.TeleportEnterPos)
                });

                session.FillCircle(WorldConstants.ToWorldCoord(Model.TeleportExitPos),  teleportRadius, new CanvasRadialGradientBrush(session.Device,
                    new CanvasGradientStop[2] { new CanvasGradientStop() { Color = Colors.Blue, Position = 0 }, new CanvasGradientStop() { Color = Colors.Black, Position = 1 } },
                    CanvasEdgeBehavior.Clamp,
                    CanvasAlphaMode.Premultiplied)
                {
                    RadiusX = teleportRadius,
                    RadiusY = teleportRadius,
                    Center = WorldConstants.ToWorldCoord(Model.TeleportExitPos)
                });
            }

            if (Model.Body.Count == 0)
                return;

            if(Model.DeathAnimationEncounter == 0)
            {
                Model.ChangeSizeAfterFoodOvereaten();
            }
            else if (Model.DeathAnimationEncounter > 0)
            {
                Model.DeathAnimationEncounter--;
            }

            Queue<Vector2> tmpQueue = new Queue<Vector2>();
            Vector2 tmpPos;
            Color circleColor;
            CanvasBitmap foodBitmap;

            
            int snakePartIndex = Model.Body.Count - 1;
            snakePart = Model.Body.Last;
            while (snakePart != null)
            {
                tmpPos = snakePart.Value.GetNextPosition();

                if (snakePart == Model.Body.First)
                    head_pos = WorldConstants.ToWorldCoord(tmpPos);

                tmpQueue.Enqueue(tmpPos);

                session.DrawImage(Food.shadowBitmap,
                            new Rect(WorldConstants.ToWorldCoord(tmpPos).X - 1.2 * SnankeRadiusWorld,
                                        WorldConstants.ToWorldCoord(tmpPos).Y - 1.2 * SnankeRadiusWorld,
                                        SnankeRadiusWorld * 2.4,
                                        SnankeRadiusWorld * 2.4),
                            new Rect(0, 0, Food.shadowBitmap.Size.Width, Food.shadowBitmap.Size.Height));

                snakePart = snakePart.Previous;
                snakePartIndex--;
            }
            
            snakePartIndex = Model.Body.Count - 1;
            snakePart = Model.Body.Last;
            while (snakePart != null)
            {
                tmpPos = tmpQueue.Dequeue();

                GetFood(snakePart.Value, snakePartIndex, drawIndex, out circleColor, out foodBitmap);

                
                if (snakePartIndex >= Model.MinLength || avoidObstaclesAlgorithm.State == SnakeState.Touched)
                    session.FillCircle(WorldConstants.ToWorldCoord(tmpPos),
                                    SnankeRadiusWorld, circleColor);
                else
                    session.FillCircle(WorldConstants.ToWorldCoord(tmpPos),
                                    SnankeRadiusWorld, (snakePartIndex) % 2 != 0 ? 
                                    Color.FromArgb(255, (byte) (0.8 * Color.R), (byte)(0.8 * Color.G), (byte)(0.8 * Color.B)) :
                                    Color);

                if ((snakePartIndex >= Model.MinLength || snakePart.Value.SwallowedFoodColorIndex != null) && foodBitmap != null)
                {
                    session.DrawImage(foodBitmap,
                                new Rect(WorldConstants.ToWorldCoord(tmpPos).X - SnankeRadiusWorld * 0.72,
                                            WorldConstants.ToWorldCoord(tmpPos).Y - SnankeRadiusWorld * 0.72,
                                            SnankeRadiusWorld * 1.44,
                                            SnankeRadiusWorld * 1.44),
                                new Rect(0, 0, foodBitmap.Size.Width, foodBitmap.Size.Height),
                                snakePart.Value.ToDelete ? Model.DeathAnimationEncounter / 50f : 1f);
                }

                snakePart = snakePart.Previous;
                snakePartIndex--;
            }


            //ToDo: move headPos to cycle
            /*
            tmpPos = Model.Body.First.Value.GetNextPosition();
            head_pos = WorldConstants.ToWorldCoord(tmpPos);

            if(avoidObstaclesAlgorithm.State == SnakeState.Touched)
            {
                GetFood(snakePart.Value, snakePartIndex, drawIndex, out circleColor, out foodBitmap);
                session.FillCircle(head_pos, SnankeRadiusWorld, circleColor);

                if (foodBitmap != null)
                    session.DrawImage(foodBitmap,
                                new Rect((head_pos).X - SnankeRadiusWorld * 0.72,
                                            (head_pos).Y - SnankeRadiusWorld * 0.72,
                                            SnankeRadiusWorld * 1.44,
                                            SnankeRadiusWorld * 1.44), 
                                new Rect(0, 0, foodBitmap.Size.Width, foodBitmap.Size.Height), 
                                snakePart.Value.ToDelete ? Model.DeathAnimationEncounter / 50f : 1f);
            }
            else
                session.FillCircle(head_pos, SnankeRadiusWorld, Color);

            */
            /*
            if (avoidObstaclesAlgorithm.route != null && avoidObstaclesAlgorithm.route.Count > 0)
            {
                foreach (var routePointPos in avoidObstaclesAlgorithm.route.Skip(1))
                {
                    session.FillCircle(WorldConstants.ToWorldCoord(routePointPos.Position),
                                                            3, Color.FromArgb(255, (byte)((avoidObstaclesAlgorithm.route.Count % 3) * 70),
                                                                                   (byte)((7 - avoidObstaclesAlgorithm.route.Count) * 30),
                                                                                   (byte)((avoidObstaclesAlgorithm.route.Count) * 20)));
                }
            }
            */

            DrawEyes(session);
        }



        protected abstract void SwallowFood(int drawIndex);

        protected abstract Color GetSnakeColor(byte colorIndex, int snakePartIndex);

        public void GetFood(SnakePart snakePart, int snakePartIndex, int drawIndex, out Color circleColor, out CanvasBitmap foodBitmap)
        {
            if (avoidObstaclesAlgorithm.State == SnakeState.Touched)
            {
                if (!touchedOpacitySign)
                    touchedOpacity += 1;
                else
                    touchedOpacity -= 1;

                if (touchedOpacity == 255 || touchedOpacity == 105)
                    touchedOpacitySign = !touchedOpacitySign;

                circleColor = snakePartIndex % 2 == 0 ? Color.FromArgb(snakePart.ToDelete ? (byte)(255 * Model.DeathAnimationEncounter / 50) : touchedOpacity, 255, 0, 0) : 
                                                        Color.FromArgb(snakePart.ToDelete ? (byte)(255 * Model.DeathAnimationEncounter / 50) : touchedOpacity, 203, 0, 0);
                foodBitmap = snakePart.ColorIndex == byte.MaxValue ? null : Food.foodBitmap_[snakePart.ColorIndex];
            }
            else if(snakePart.ToDelete)
            {
                circleColor = snakePartIndex % 2 == 0 ? Color.FromArgb((byte)(255 * Model.DeathAnimationEncounter / 50), 255, 255, 255)
                                                      : Color.FromArgb((byte)(255 * Model.DeathAnimationEncounter / 50), 203, 203, 203);
                foodBitmap = snakePart.ColorIndex == byte.MaxValue ? null : Food.foodBitmap[snakePart.ColorIndex];
            }
            else if (snakePart.SwallowedFoodColorIndex == null)
            {
                circleColor = snakePartIndex % 2 == 0 ? Colors.White : Color.FromArgb(255, 203, 203, 203);
                foodBitmap = snakePart.ColorIndex == byte.MaxValue ? null : Food.foodBitmap[snakePart.ColorIndex];
            }
            else if (snakePart.SwallowedFoodColorIndex != snakePart.ColorIndex)
            {
                circleColor = snakePart.ColorIndex == byte.MaxValue ? Colors.Black : Food.PossibleColors[snakePart.ColorIndex];
                foodBitmap = Food.foodBitmap_[(byte)snakePart.SwallowedFoodColorIndex];
            }
            else
            {
                if (snakePart.FoodStuckedAnimationEncounter <= 0)
                {
                    circleColor = Food.PossibleColors[snakePart.ColorIndex];
                    foodBitmap = Food.foodBitmap_[snakePart.ColorIndex];

                    if(snakePart.FoodStuckedAnimationEncounter == 0)
                    {
                        snakePart.SwallowedFoodColorIndex = null;
                        snakePart.FoodStuckedAnimationEncounter = -1;
                    }
                    return;
                }

                if(snakePart.FoodStuckedAnimationEncounter % 20 < 10)
                {
                    circleColor = GetSnakeColor(snakePart.ColorIndex, snakePartIndex);
                    foodBitmap = Food.foodBitmap_[snakePart.ColorIndex];
                }
                else
                {
                    circleColor = Colors.Red;
                    foodBitmap = Food.foodBitmap_[(byte)snakePart.SwallowedFoodColorIndex];
                }
                snakePart.FoodStuckedAnimationEncounter--;
            }
        }

        public void DrawEyes(CanvasDrawingSession session)
        {
            double angleBetweenEyes = Math.PI / 2;
            float eyePupilRadius = SnankeRadiusWorld * 0.45f;

            double newEyeWhiteAngle;

            newEyeWhiteAngle = NewAngle;
            
            while (newEyeWhiteAngle - OldAngle > Math.PI / 2)
                newEyeWhiteAngle -= Math.PI;

            while (newEyeWhiteAngle - OldAngle < -Math.PI / 2)
                newEyeWhiteAngle += Math.PI;


            newEyeWhiteAngle = OldAngle + (newEyeWhiteAngle - OldAngle) * drawIndex / SnakeModel.PathLength + Math.PI / 2;


            Vector2 rightWhiteOfEye = new Vector2(head_pos.X + (float)(eyePupilRadius * Math.Sin(newEyeWhiteAngle - angleBetweenEyes)),
                                                  head_pos.Y - (float)(eyePupilRadius * Math.Cos(newEyeWhiteAngle - angleBetweenEyes)));

            Vector2 leftWhiteOfEye  = new Vector2(head_pos.X + (float)(eyePupilRadius * Math.Sin(newEyeWhiteAngle + angleBetweenEyes)),
                                                  head_pos.Y - (float)(eyePupilRadius * Math.Cos(newEyeWhiteAngle + angleBetweenEyes)));

            session.FillCircle(rightWhiteOfEye, eyePupilRadius * 0.2f, Colors.Black);
            session.FillCircle(leftWhiteOfEye , eyePupilRadius * 0.2f, Colors.Black);

            if (drawIndex == SnakeModel.PathLength) drawIndex = 0;
        }

        public bool IsSnakeStopped;
        public void Stop()
        {
            Model.Stop();
            avoidObstaclesAlgorithm.route.RemoveRange(1, avoidObstaclesAlgorithm.route.Count - 2);
        }
    }
}
