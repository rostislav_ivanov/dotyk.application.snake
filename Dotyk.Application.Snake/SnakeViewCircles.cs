﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public class SnakeViewCircles : SnakeView
    {
        public SnakeViewCircles(bool isGoodsnake, LinkedList<SnakePart> newBody = null) : base(false, isGoodsnake, newBody) { }

        bool touchedOpacitySign;
        byte touchedOpacity = 180;

        protected override void SwallowFood(int drawIndex)
        {
            if (drawIndex % 8 == 0)
            {
                Model.MoveFood();
            }
        }

        protected override Color GetSnakeColor(byte colorIndex, int snakePartIndex)
        {
            Color currSnakePartColor = Food.PossibleColors[colorIndex];

            if (avoidObstaclesAlgorithm.State == SnakeState.Touched)
            {
                if (!touchedOpacitySign)
                    touchedOpacity += 1;
                else
                    touchedOpacity -= 1;

                if (touchedOpacity == 255 || touchedOpacity == 105)
                    touchedOpacitySign = !touchedOpacitySign;

                return snakePartIndex % 2 == 0 ? Color.FromArgb(touchedOpacity, 255, 0, 0) : Color.FromArgb(touchedOpacity, 255, 255, 255);
            }
            /*else if (avoidObstaclesAlgorithm.State == SnakeState.Captured)
            {
                return Colors.Green;
            }*/
            else
            {
                return currSnakePartColor;
            }
        }

        protected override void UpdateViewColors(byte? isFoodEaten)
        {
            if (isFoodEaten != null && Model.IsGoodSnake)
            {
                Model.SwallowFood((byte)isFoodEaten);
            }
        }
     }
}
