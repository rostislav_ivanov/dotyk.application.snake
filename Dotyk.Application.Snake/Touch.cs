﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Runtime.InteropServices;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public class Touch
    {
        public bool IsInited;
        private static int Width = 189;
        private static int Height = 110;
        public static int RealWidth = 189;
        public static int RealHeight = 110;
        public static bool IsDuos;
        public static bool IsBar;

        private const int MaxThreshOld = 8000;
        private unsafe int* _accumulated;
        private unsafe short* _texturePtr;

        private unsafe int* _matrixIndexPtr;
        private int _index0;
        private int _value;

        private int _arraySize;
        private int _index;
        private short _threshold;
        private IntPtr _previousIntPtr;
        private unsafe short* _previousPtr;
        private IntPtr _resultIntPtr;
        private unsafe short* _resultPtr;
        private IntPtr _textureCopyIntPtr;
        private unsafe short* _textureCopyPtr;
        private IntPtr _textureIntPtr;
        private IntPtr _resultErodeIntPtr;
        private unsafe short* _resultErodePtr;

        Matrix4x4 _transformMatrixTexture;
        public Matrix4x4 _transformMatrixScreen;
        Matrix4x4 _transformMatrixScreenInvert;

        public Vector2 TransformMatrixScreenInvert(Vector2 worldCoord)
        {
            float x = WorldConstants.Width - worldCoord.X;
            float y = worldCoord.Y;

            var x1 = x * _transformMatrixScreenInvert.M11 + y * _transformMatrixScreenInvert.M21 + _transformMatrixScreenInvert.M41;
            var y1 = x * _transformMatrixScreenInvert.M12 + y * _transformMatrixScreenInvert.M22 + _transformMatrixScreenInvert.M42;

            var z1 = x * _transformMatrixScreenInvert.M14 + y * _transformMatrixScreenInvert.M24 + _transformMatrixScreenInvert.M44;

            x = (x1 / z1);
            y = (y1 / z1);

            if (x < 0 || x >= WorldConstants.Width || y < 0 || y >= WorldConstants.Height)
            {
                x = -1;
                y = -1;
            }

            return new Vector2(x, y);
        }

        public Vector2 TransformMatrixScreen(Vector2 touchCoord)
        {
            float x = touchCoord.X;
            float y = touchCoord.Y;

            var x1 = x * _transformMatrixScreen.M11 + y * _transformMatrixScreen.M21 + _transformMatrixScreen.M41;
            var y1 = x * _transformMatrixScreen.M12 + y * _transformMatrixScreen.M22 + _transformMatrixScreen.M42;

            var z1 = x * _transformMatrixScreen.M14 + y * _transformMatrixScreen.M24 + _transformMatrixScreen.M44;

            x = WorldConstants.Width - (x1 / z1);
            y = (y1 / z1);

            if (x < 0 || x >= WorldConstants.Width || y < 0 || y >= WorldConstants.Height)
            {
                x = -1;
                y = -1;
            }

            return new Vector2(x,y);
        }

        public int TransformMatrixTexture(int x, int y)
        {
            var x1 = x * _transformMatrixTexture.M11 + y * _transformMatrixTexture.M21 + _transformMatrixTexture.M41;
            var y1 = x * _transformMatrixTexture.M12 + y * _transformMatrixTexture.M22 + _transformMatrixTexture.M42;

            var z1 = x * _transformMatrixTexture.M14 + y * _transformMatrixTexture.M24 + _transformMatrixTexture.M44;

            x = (int)(x1 / z1);
            y = (int)(y1 / z1);

            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                x = -1;
                y = -1;
            }
            return x * Height + y;
        }

        public event EventHandler KoSenseInited;
        public unsafe void KoSenseInit()
        {
            var matrix = DotykApplication.Current.ShellClient.KoSense.Transformation * Matrix4x4.CreateScale(RealWidth, RealHeight, 1);
            _transformMatrixTexture = new Matrix4x4();

            Matrix4x4.Invert(matrix, out _transformMatrixTexture);

            _transformMatrixScreen = new Matrix4x4();
            _transformMatrixScreen = DotykApplication.Current.ShellClient.KoSense.Transformation * Matrix4x4.CreateScale(WorldConstants.Width, WorldConstants.Height, 1);
            Matrix4x4.Invert(_transformMatrixScreen, out _transformMatrixScreenInvert);

            if (DotykApplication.Current.ShellClient.KoSense.Transformation.M11 == 0)
                return;
            else
                IsInited = true;

            KoSenseInited?.Invoke(null, null);

            _textureIntPtr = DotykApplication.Current.ShellClient.KoSense.Diff;
            _texturePtr = (short*)_textureIntPtr.ToPointer();

            _threshold = (short)(DotykApplication.Current.ShellClient.KoSense.SensorDescription.Threshold);
            _arraySize = Height * Width;
            _matrixIndexPtr = (int*)Marshal.AllocHGlobal(sizeof(int) * _arraySize).ToPointer();

            for (var i = 0; i < Width; i++)
                for (var j = 0; j < Height; j++)
                    *(_matrixIndexPtr + j * Width + i) = TransformMatrixTexture(i, j);

            _textureCopyIntPtr = Marshal.AllocHGlobal(sizeof(short) * _arraySize);
            _textureCopyPtr = (short*)_textureCopyIntPtr.ToPointer();

            _previousIntPtr = Marshal.AllocHGlobal(sizeof(short) * _arraySize);
            _previousPtr = (short*)_previousIntPtr.ToPointer();

            var p = Marshal.AllocHGlobal(sizeof(int) * _arraySize);
            _accumulated = (int*)p.ToPointer();

            _resultIntPtr = Marshal.AllocHGlobal(sizeof(short) * _arraySize);
            _resultPtr = (short*)_resultIntPtr.ToPointer();

            _resultErodeIntPtr = Marshal.AllocHGlobal(sizeof(short) * _arraySize);
            _resultErodePtr = (short*)_resultErodeIntPtr.ToPointer();
        }

        int obstacalesIndex = 0;
        //const int obstacalesCount = 12;
        long[] touchSum = new long[RealWidth * RealHeight];
        
        public event Action EnoughTouchesCollected;

        public Color[] ConvertTouchToColor(int treshold1, int treshold2, int treshold3, bool deleteBool= false)
        {
            short treshold = (short)(DotykApplication.Current.ShellClient.KoSense.SensorDescription.Threshold);

            Color[] result = new Color[RealWidth * RealHeight];

            if (obstacalesIndex == 0)
            {
                //ToDo: make it faster without cycle
                for (int j = 0; j < RealWidth * RealHeight; j++)
                {
                    result[j] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                }

                return result;
            }

            byte[] forCanny = new byte[RealWidth * RealHeight];

            long tmp_l;

            int index;

            int startObstacleVerticalIndex;
            bool previousValue; //isObstacle = true

            for (int i = 0; i < RealWidth; i++)    
            {
                startObstacleVerticalIndex = -1;
                previousValue = false;
                for (int j = 0; j < RealHeight; j++)
                {
                    index = i + j * RealWidth;
                    tmp_l = touchSum[index] / obstacalesIndex;

                    if (Math.Abs(tmp_l) >= treshold)
                    {
                        if(!previousValue)
                        {
                            startObstacleVerticalIndex = j;
                        }

                        result[index] = AvoidObstaclesAlgorithm.ObstacleColor;
                        previousValue = true;
                    }
                    else
                    {
                        if (previousValue)
                        {
                            if(startObstacleVerticalIndex != -1)
                            {
                                if (j - startObstacleVerticalIndex > 7)
                                {
                                    result[i + (startObstacleVerticalIndex) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (startObstacleVerticalIndex + 1) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (startObstacleVerticalIndex + 2) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (j - 1) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (j - 2) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (j - 3) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    startObstacleVerticalIndex = -1;
                                }
                                else if (j - startObstacleVerticalIndex > 5)
                                {
                                    result[i + (startObstacleVerticalIndex) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (startObstacleVerticalIndex + 1) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (j - 1)* RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    result[i + (j - 2) * RealWidth] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                                    startObstacleVerticalIndex = -1;
                                }
                            }
                        }

                        result[index] = AvoidObstaclesAlgorithm.EmptySpaceColor;
                        previousValue = false;
                    }

                    touchSum[index] = 0;
                }
            }

            obstacalesIndex = 0;

            return result;
        }

        public void GetTouch()
        {
            short treshold = (short)( DotykApplication.Current.ShellClient.KoSense.SensorDescription.Threshold);
            try
            {
                if (!IsInited) return;
                unsafe
                {
                    #region Old for whole texture

                    for (var i = 0; i < RealWidth; i++)
                    {
                        for (var j = 0; j < RealHeight; j++)
                        {
                            _index = j * Width  + i ;

                            _value = *(_texturePtr + *(_matrixIndexPtr + _index));
                            //*(_previousPtr + _index) = *(_texturePtr + *(_matrixIndexPtr + _index));

                            if (_value < 0) _value = 0;

                            if ((_value) > treshold)
                                _value = treshold;

                            //*(_resultPtr + _index) = 

                            touchSum[j * RealWidth + i] += (short)_value;
                        }
                    }
                            
                    //Erode(_resultPtr, _textureCopyPtr, _arraySize, Width);
                    //Delitate(_textureCopyPtr, _resultPtr, _arraySize, Width);

                    /*for (_index = 0; _index < RealWidth * RealHeight; _index++)
                    {
                        //int new_index = (_index / RealWidth) * RealWidth;
                        //touchSum[Width - 1 - _index + 2 * new_index] += (byte)(255 - (byte)*(_resultPtr + _index));
                        touchSum[_index] += *(_resultPtr + _index);
                    }*/

                    #endregion
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            obstacalesIndex++;

            if (obstacalesIndex >= 5)
                EnoughTouchesCollected?.Invoke();
            //return obstacales;
        }

        #region Erode&Delitate
        private int _erodeI;
        private int _erodeJ;
        private int _erodeX;
        private int _erodeY;
        private int _erodeXL;
        private int _erodeYL;
        private int _erodeWidth;
        private int _erodeHeight;
        private readonly int[][] _erodeCore =
            {
            new[] { 0, 1, 0 },
            new[] { 0, 1, 0 },
            new[] { 0, 1, 0 }
        };

        private readonly int[][] _delitateCore =
        {
            new[] { 1, 1, 1 },
            new[] { 1, 1, 1 },
            new[] { 1, 1, 1 }
        };
        private readonly int[] _erodeIndexes = { -1, 0, 1 };
        private int _erodeIndex;
        private int _erodeIndexG;
        private bool _clear;
        private unsafe void Erode(short* textureSourcePtr, short* textureDestPtr, int textureSize, int pitch)
        {
            _erodeHeight = textureSize / pitch;
            _erodeWidth = pitch;
            for (_erodeI = 0; _erodeI < _erodeWidth; _erodeI++)
            {
                for (_erodeJ = 0; _erodeJ < _erodeHeight; _erodeJ++)
                {
                    _clear = false;
                    _erodeIndexG = _erodeJ * _erodeWidth + _erodeI;
                    if (*(textureSourcePtr + _erodeIndexG) == 0)
                    {
                        *(textureDestPtr + _erodeIndexG) = 0;
                        continue;
                    }
                    for (_erodeX = 0; _erodeX < _erodeCore.Length && !_clear; _erodeX++)
                    {
                        for (_erodeY = 0; _erodeY < _erodeCore[_erodeX].Length && !_clear; _erodeY++)
                        {
                            _erodeXL = _erodeI + _erodeIndexes[_erodeX];
                            _erodeYL = _erodeJ + _erodeIndexes[_erodeY];
                            if (_erodeXL < 0 || _erodeXL >= _erodeWidth || _erodeYL < 0 || _erodeYL >= _erodeHeight) continue;
                            if (_erodeCore[_erodeX][_erodeY] == 0) continue;
                            _erodeIndex = _erodeXL + _erodeYL * _erodeWidth;
                            if (*(textureSourcePtr + _erodeIndex) == 0)
                                _clear = true;
                        }
                    }
                    *(textureDestPtr + _erodeIndexG) = !_clear ? *(textureSourcePtr + _erodeIndexG) : (short)0;
                }
            }
        }

        private unsafe void Delitate(short* textureSourcePtr, short* textureDestPtr, int textureSize, int pitch)
        {
            _erodeHeight = textureSize / pitch;
            _erodeWidth = pitch;
            for (_erodeI = 0; _erodeI < _erodeWidth; _erodeI++)
            {
                for (_erodeJ = 0; _erodeJ < _erodeHeight; _erodeJ++)
                {
                    _erodeIndexG = _erodeJ * _erodeWidth + _erodeI;
                    if (*(textureSourcePtr + _erodeIndexG) == 0)
                    {
                        *(textureDestPtr + _erodeIndexG) = 0;
                        continue;
                    }
                    for (_erodeX = 0; _erodeX < _delitateCore.Length && !_clear; _erodeX++)
                    {
                        for (_erodeY = 0; _erodeY < _delitateCore[_erodeX].Length && !_clear; _erodeY++)
                        {
                            _erodeXL = _erodeI + _erodeIndexes[_erodeX];
                            _erodeYL = _erodeJ + _erodeIndexes[_erodeY];
                            if (_erodeXL < 0 || _erodeXL >= _erodeWidth || _erodeYL < 0 || _erodeYL >= _erodeHeight) continue;
                            if (_delitateCore[_erodeX][_erodeY] == 0) continue;
                            _erodeIndex = _erodeXL + _erodeYL * _erodeWidth;
                            *(textureDestPtr + _erodeIndex) = 255;
                        }
                    }
                }
            }
        }

        #endregion

        public static Color[] TouchSimulation()
        {
            int objectsCount = 0;
            int objectMaxSize = 20;
            Random random = new Random();

            Color[] obstacales = new Color[RealWidth * RealHeight];

            for (int i = 0; i < obstacales.Length; i++)
            {
                obstacales[i] = AvoidObstaclesAlgorithm.EmptySpaceColor;
            }

            var objets = new Vector2[objectsCount];
            int radiusOfIntereset;
            int xMinValue;
            int xMaxValue;
            int yMinValue;
            int yMaxValue;
            double chance;

            for (int k = 0; k < objets.Length; k++)
            {
                objets[k] = new Vector2((int)(random.NextDouble() * RealWidth),
                                        (int)(random.NextDouble() * RealHeight));

                radiusOfIntereset = (int)(random.NextDouble() * objectMaxSize);

                xMinValue = (int)Math.Max(0, objets[k].X - radiusOfIntereset);
                xMaxValue = (int)Math.Min(RealWidth, objets[k].X + radiusOfIntereset);
                yMinValue = (int)Math.Max(0, objets[k].Y - radiusOfIntereset);
                yMaxValue = (int)Math.Min(RealHeight, objets[k].Y + radiusOfIntereset);
                for (int i = xMinValue; i < xMaxValue; i++)
                {
                    for (int j = yMinValue; j < yMaxValue; j++)
                    {
                        obstacales[j * RealWidth + i] = (int)Math.Sqrt((objets[k].X - i) * (objets[k].X - i) +
                                                    (objets[k].Y - j) * (objets[k].Y - j)) == radiusOfIntereset ? AvoidObstaclesAlgorithm.ObstacleColor : AvoidObstaclesAlgorithm.EmptySpaceColor;
                        /*
                        chance = 1.0 - 0.5 *(Math.Sqrt( (objets[k].X-i)* (objets[k].X - i) + 
                                                    (objets[k].Y - i) * (objets[k].Y - i)) / (radiusOfIntereset*Math.Sqrt(2)));
                        colors[i* simulationH + j] = (random.NextDouble() < chance) ? Colors.Black : Colors.Transparent;
                        */
                    }
                }
            }

            return obstacales;
        }

        #region Canny

        static int[,] SobelKernelDx = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
        static int[,] SobelKernelDy = { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };

        static byte[] GetCannyResult(byte[] src, int srcWidth, int srcHeight, double lowThresh, double highThresh)
        {
            const int cannyShift = 15;
            const int tg22 = (int)(0.4142135623730950488016887242097 * (1 << cannyShift) + 0.5);

            var dst = new byte[srcWidth * srcHeight];
            var low = (int)Math.Floor(lowThresh);
            var high = (int)Math.Floor(highThresh);

            var mapstep = srcWidth + 2;
            var buffer = new int[mapstep * (srcHeight + 2)];

            var magBuf = new int[mapstep];
            var magBuf1 = new int[mapstep];
            var magBuf2 = new int[mapstep];

            var lastRowIndex = mapstep * (srcHeight + 1);
            for (int i = 0; i < mapstep; i++)
            {
                buffer[i] = 1;
                buffer[lastRowIndex + i] = 1;
            }

            for (int j = 0; j < (srcHeight + 2); j++)
            {
                buffer[j * mapstep] = 1;
                buffer[j * mapstep + mapstep - 1] = 1;
            }

            var stack = new Stack<int>();
            int indexForStack;

            var dx = Touch.Convolute(src, srcWidth, srcHeight, Touch.SobelKernelDx);
            var dy = Touch.Convolute(src, srcWidth, srcHeight, Touch.SobelKernelDy);

            // calculate magnitude and angle of gradient, perform non-maxima supression. fill the map with one of the following values:
            //   0 - the pixel might belong to an edge
            //   1 - the pixel can not belong to an edge
            //   2 - the pixel does belong to an edge
            for (int i = 0; i <= srcHeight; i++)
            {
                var norm = i == 0 ? magBuf1 : magBuf2;

                if (i < srcHeight)
                {
                    for (int j = 0; j < srcWidth; j++)
                    {
                        var index = j + i * srcWidth;
                        norm[1 + j] = (Math.Abs(dx[index]) + Math.Abs(dy[index]));
                    }

                    norm[0] = 0;
                    norm[1 + srcWidth] = 0;
                }
                else for (int j = 0; j < mapstep; j++) norm[j] = 0;

                // at the very beginning we do not have a complete ring buffer of 3 magnitude rows for non-maxima suppression
                if (i == 0) continue;

                var map = mapstep * i + 1;
                var mag = magBuf1; // take the central row

                var prevFlag = false;
                for (int j = 0; j < srcWidth; j++)
                {
                    var m = mag[1 + j];

                    if (m > low)
                    {
                        var index = j + (i - 1) * srcWidth;
                        var xs = dx[index];
                        var ys = dy[index];
                        var x = Math.Abs(xs);
                        var y = Math.Abs(ys) << cannyShift;

                        var tg22X = x * tg22;

                        if (y < tg22X)
                        {
                            if (m > mag[j] && m >= mag[j + 2])
                                goto __ocv_canny_push;
                        }
                        else
                        {
                            int tg67X = tg22X + (x << (cannyShift + 1));
                            if (y > tg67X)
                            {
                                if (m > magBuf[1 + j] && m >= magBuf2[1 + j])
                                    goto __ocv_canny_push;
                            }
                            else
                            {
                                int s = (xs ^ ys) < 0 ? -1 : 1;
                                if (m > magBuf[1 + j - s] && m > magBuf2[1 + j + s])
                                    goto __ocv_canny_push;
                            }
                        }
                    }

                    prevFlag = false;
                    buffer[map + j] = 1;
                    continue;

                    __ocv_canny_push:
                    if (!prevFlag && m > high && buffer[map + j - mapstep] != 2)
                    {
                        indexForStack = map + j; buffer[indexForStack] = 2; stack.Push(indexForStack);
                        prevFlag = true;
                    }
                    else buffer[map + j] = 0;
                }

                // scroll the ring buffer
                mag = magBuf;
                magBuf = magBuf1;
                magBuf1 = magBuf2;
                magBuf2 = mag;
            }

            // now track the edges (hysteresis thresholding)
            while (stack.Count > 0)
            {
                var m = stack.Pop();

                if (buffer[m - 1] == 0) { indexForStack = m - 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m + 1] == 0) { indexForStack = m + 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m - mapstep - 1] == 0) { indexForStack = m - mapstep - 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m - mapstep] == 0) { indexForStack = m - mapstep; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m - mapstep + 1] == 0) { indexForStack = m - mapstep + 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m + mapstep - 1] == 0) { indexForStack = m + mapstep - 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m + mapstep] == 0) { indexForStack = m + mapstep; buffer[indexForStack] = 2; stack.Push(indexForStack); }
                if (buffer[m + mapstep + 1] == 0) { indexForStack = m + mapstep + 1; buffer[indexForStack] = 2; stack.Push(indexForStack); }
            }

            // the final pass, form the final image
            var pmap = mapstep + 1;

            for (int i = 0; i < srcHeight; i++, pmap += mapstep)
            {
                for (int j = 0; j < srcWidth; j++)
                    dst[j + i * srcWidth] = (byte)-(buffer[pmap + j] >> 1);
            }

            return dst;
        }
       

        public static int[] Convolute(byte[] src, int srcWidth, int srcHeight, int[,] kernel, int kernelOffsetSum = 0)
        {
            var kernelFactorSum = 0;
            foreach (var b in kernel)
                kernelFactorSum += b;

            var kh = kernel.GetUpperBound(0) + 1;
            var kw = kernel.GetUpperBound(1) + 1;

            var result = new int[src.Length];
            var index = 0;
            var kwh = kw >> 1;
            var khh = kh >> 1;

            for (var y = 0; y < srcHeight; y++)
            {
                for (var x = 0; x < srcWidth; x++)
                {
                    var p = 0;

                    for (var kx = -kwh; kx <= kwh; kx++)
                    {
                        var px = kx + x;
                        // Repeat pixels at borders
                        if (px < 0) px = 0;
                        else if (px >= srcWidth) px = srcWidth - 1;

                        for (var ky = -khh; ky <= khh; ky++)
                        {
                            var py = ky + y;
                            // Repeat pixels at borders
                            if (py < 0) py = 0;
                            else if (py >= srcHeight) py = srcHeight - 1;

                            var col = src[py * srcWidth + px];
                            var k = kernel[ky + kwh, kx + khh];
                            p += (col & 0xFF) * k;
                        }
                    }

                    result[index++] = kernelFactorSum > 0 ? ((p / kernelFactorSum) + kernelOffsetSum) : p;
                }
            }

            return result;
        }

        public static void BlurFastHorizontal(ref byte[] pixels, int range)
        {
            int w = RealWidth;
            int h = RealHeight;
            int halfRange = range / 2;
            int index = 0;
            var newColors = new byte[w];

            for (int y = 0; y < h; y++)
            {
                int hits = 0;
                int tmp = 0;
                for (int x = -halfRange; x < w; x++)
                {
                    int oldPixel = x - halfRange - 1;
                    if (oldPixel >= 0)
                    {
                        tmp -= pixels[index + oldPixel];
                        hits--;
                    }

                    int newPixel = x + halfRange;
                    if (newPixel < w)
                    {
                        tmp += pixels[index + newPixel];
                        hits++;
                    }

                    if (x >= 0)
                    { 
                        newColors[x] = (byte)(tmp / hits);
                    }
                }

                for (int x = 0; x < w; x++)
                {
                    pixels[index + x] = newColors[x];
                }

                index += w;
            }
        }

        public static void BlurFastVertical(ref byte[] pixels, int range)
        {
            int w = RealWidth;
            int h = RealHeight;
            int halfRange = range / 2;

            var newColors = new byte[h];
            int oldPixelOffset = -(halfRange + 1) * w;
            int newPixelOffset = (halfRange) * w;

            for (int x = 0; x < w; x++)
            {
                int hits = 0;
                int tmp = 0;
                int index = -halfRange * w + x;
                for (int y = -halfRange; y < h; y++)
                {
                    int oldPixel = y - halfRange - 1;
                    if (oldPixel >= 0)
                    {
                        tmp -= pixels[index + oldPixelOffset];
                        hits--;
                    }

                    int newPixel = y + halfRange;
                    if (newPixel < h)
                    {
                        tmp += pixels[index + newPixelOffset];
                        hits++;
                    }

                    if (y >= 0)
                    {
                        newColors[y] = (byte)(tmp / hits);
                    }

                    index += w;
                }

                for (int y = 0; y < h; y++)
                {
                    pixels[y * w + x] = newColors[y];
                }
            }
        }

        #endregion
    }
}
