﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI;

namespace Dotyk.Application.Snake
{
    public static class Utils
    {
        public static async Task<byte[]> ReadAllBytes(string filename)
        {
            var uri = new Uri("ms-appx:///" + filename);
            var file = await StorageFile.GetFileFromApplicationUriAsync(uri);
            var buffer = await FileIO.ReadBufferAsync(file);

            return buffer.ToArray();
        }

        public static Random Rand;
    }

    public struct MinMaxRegion  //float Rect
    {
        public Vector2 MinPoint;
        public Vector2 MaxPoint;

        public MinMaxRegion(Vector2 minPoint, Vector2 maxPoint)
        {
            MinPoint = minPoint;
            MaxPoint = maxPoint;
        }

        public float Width
        {
            get { return MaxPoint.X - MinPoint.X; }
        }

        public float Height
        {
            get { return MaxPoint.Y - MinPoint.Y; }
        }
    }
}
