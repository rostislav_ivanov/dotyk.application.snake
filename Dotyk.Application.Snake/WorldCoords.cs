﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Snake
{
    public static class WorldConstants
    {
        //public const float snakeSpeed = 4;
        //public const float one_div_snakeSpeed = 1 / snakeSpeed;

        public static int Width;
        public static int Height;
        public static float w_koeff;
        public static float h_koeff;

        

        public const double maxAngle = 20 * Math.PI;


        public static float TeleportFullSize;

        public static Touch TouchObj = new Touch();
        public static Vector2 ToWorldCoord(Vector2 touchCoord)
        {
            //if(TouchObj.IsInited)
            //    return TouchObj.TransformMatrixScreen(touchCoord);
            //else
                return new Vector2(touchCoord.X * w_koeff, touchCoord.Y * h_koeff);
            
        }

        public static Vector2 ToTouchCoord(Vector2 worldCoord)
        {
            //if (TouchObj.IsInited)
            //    return TouchObj.TransformMatrixScreenInvert(worldCoord);
            //else
                return new Vector2(worldCoord.X / w_koeff, worldCoord.Y / h_koeff);
        }
    }
}
